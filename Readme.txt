  #################################
 #	MEMORY MICRO-DEMO	 # 
#################################

	Dependencies
	############
	  To compile this project you must have the following dependencies:

	OGRE 1.8.0 <http://www.ogre3d.org/>
	Cg Toolkit 3.0  <http://developer.nvidia.com/cg-toolkit/>
	OIS 1.3 <http://sourceforge.net/projects/wgois/>
	SDL 1.2 http://www.libsdl.org/projects/SDL_mixer/release-1.2.html
	CEGUI 0.8.3 http://cegui.org.uk/download
	XERCES 3.1.1 http://xerces.apache.org/mirrors.cgi
	Boost	1.49 http://www.boost.org/users/download/

	Installation
	############
	  To install you will need to download the source from the bitbucket repository.
	
	https://bitbucket.org/onepixelsoft/aerofighters.git 
	Git will be necessary to be installed, however.
	
	 
	  Write in the terminal the following commands inside the Memory folder:

	$ make
	$ ./main

	Uninstall
	###########
	  
	  For deleting all the binaries you exec the next command in the terminal:
	$ make clean

	
	GAME
	####

	  We control a spaceship as a chracter that we have to save the world from the Alien invasion we are suffering
	and has made us clear they are not coming in peace. 

	KEYBOARD
	######

	  We must use the keyboard in order to play the game.

	  Cursors: Shall be used to move and displace the airship.

	  Space bar: Shall be used to fire and shoot the enemy.

	  ESC: Shall be used to save the Game State, pause, go back to the menu or exit the game.	  


	Scoring
	##########

	The little stone will score: 		20  pts
	The asteroid will score: 		100 pts.
	The Enemy Fighters will score:		300 pts.
	The Boss Enemy will score:		5000 pts.
	
	Easy Level:		Damage 5	Speed 1x
	Normal Level:		Damage 10	Speed 2x
	Hard Level:  		Damage 20	Speed 3x
	Extreme Level:  	Damage 50	Speed 4x

	
	 
	REPOSITORY
	###########

	  To download this project from the repository use the next command in the terminal:

	user@host:$ git clone https://bitbucket.org/onepixelsoft/aerofighters.git

	NOTE: Git will be necessary to have installed as we mention earlier.
