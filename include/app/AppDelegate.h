#include <Ogre.h>
#include "factory/GameManager.h"


class AppDelegate {
  
private:
  Ogre::Root* _root;
  GameManager* _gameManager;
  
  
public:
  AppDelegate();
  ~AppDelegate();  
  int start();
  
};
