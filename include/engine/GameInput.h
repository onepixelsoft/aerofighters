#ifndef __GameInput_H__ 
#define __GameInput_H__ 

#ifndef __RenderWindow_H__  
#include <OgreRenderWindow.h>
#endif

#ifndef _Ogre_H__
#include <Ogre.h>
#endif
  
#include <OIS/OIS.h>
#include <Ogre.h>
#include "tools/GameUtil.h"
#include <CEGUI.h>

class GameListener;
class GameManager;
class Timer;

using namespace OIS;

class GameInput : MouseListener, KeyListener {
private:
	
	InputManager* _inputManager;
	Keyboard* _keyboard;
	Mouse* _mouse;
	Ogre::SceneManager* _sceneManager;
	Ogre::RenderWindow* _win;
	GameListener* _gameListener;
	GameManager* _gameManager;
	Ogre::MovableObject* _inObj;
	Timer* _timer;

public:

	GameInput(Ogre::RenderWindow *win, Ogre::SceneManager *sceneManager, GameManager* _gameManager);
	~GameInput();
	OIS::Mouse* getOisMouse() const;
	void read(const Ogre::FrameEvent& evt, Timer* timer);
	bool quit(const CEGUI::EventArgs &e);
	bool keyPressed(const OIS::KeyEvent& evt); 
	bool keyReleased(const OIS::KeyEvent& evt);
	bool mousePressed(const OIS::MouseEvent& evt, OIS ::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent& evt, OIS ::MouseButtonID id);
	bool mouseMoved(const OIS::MouseEvent &);
	void setGameListener(GameListener* gameListener);
	CEGUI::MouseButton convertMouseButton(OIS:: MouseButtonID id);
	GameManager* getGameManager() const {return _gameManager;};
	
};

#endif
