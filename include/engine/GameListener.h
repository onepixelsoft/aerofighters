#ifndef __GameListener_H__  
#define __GameListener_H__  

#ifndef __RenderWindow_H__  
#include <OgreRenderWindow.h>
#endif

#include <Ogre.h>
#include <OgreFrameListener.h>
#include "game/GameState.h"

class GameInput;
class GameObject;
class GameManager;
class Quadtree;
class Timer;

class GameListener : public Ogre::FrameListener {
private:
  
	Ogre::SceneManager* _sceneManager;
 	GameInput *_gameInput;
	GameState* _gameState;
	Quadtree* _quadtree;
	Timer* _timer;
	Ogre::SceneNode* _scene;
	Ogre::SceneNode* _layerScene;
	
  
public:
	bool _removeAll;	
  GameListener(Ogre::RenderWindow* win, Ogre::SceneManager* sceneManager);
  ~GameListener();
  bool frameStarted(const Ogre::FrameEvent& evt);
  bool frameRenderingQueued(const Ogre::FrameEvent& evt);
  void setGameInput(GameInput *gIn);
  GameInput* getGameInput() const{return _gameInput;};
  Quadtree* getQuadtree() const {return _quadtree;};
  void removeAll();
  Ogre::SceneNode* getScene() const {return _scene;};
  Ogre::SceneNode* getLayerScene() const {return _layerScene;};

};

#endif
