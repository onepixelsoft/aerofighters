#ifndef __GameObject_H__
#define __GameObject_H__

#ifndef __RenderWindow_H__  
#include <OgreRenderWindow.h>
#endif

#ifndef _Ogre_H__
#include <Ogre.h>
#endif

#include <OIS/OIS.h>

class Timer;

class GameObject {


protected:
	Ogre::Entity* _ent;
	Ogre::SceneNode* _sceneNode;
	
	
public:
	/*Var*/
	short _type;
	/*Si quieres actulizar su posicion en el quadtree, por ejemplo un objeto que no va a tener movimiento pero si colision Por defecto true*/
	bool _update_quadtree;
	/*se actuliza en el quadtree pero no queremos que colisiones con otra entidad Por defecto true*/
	bool _isIntersect;
	/* No queremos que se evalue en el game listener Por defecto true*/
	bool _remove;
	/* No queremos que se evalue en el game listener Por defecto false*/
	bool _removeAll;
	bool _isUpdate;

	GameObject* _parent;
	/*Func*/
	GameObject();
	GameObject(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	~GameObject();
	virtual void onclick();
	virtual void mouseIn();
	virtual void mouseOver();
	virtual void mouseOut();

	virtual Ogre::Entity* getEntity() const{return _ent;};
	virtual Ogre::SceneNode* getSceneNode() const{return _sceneNode;};

	virtual void update(const Ogre::FrameEvent& evt, Timer* timer);
	virtual void keyboard(OIS::Keyboard* keyboard, Timer* timer);
	virtual Ogre::AxisAlignedBox getBox() const {return _sceneNode->_getWorldAABB();};
	virtual float getX() const;
	virtual float getY() const;
	virtual float getWidth() const;
	virtual float getHeight() const;
	virtual bool getPosCenter(Ogre::Vector2& result);
	virtual bool getPosAll(Ogre::Vector2& nlt, Ogre::Vector2& nrt, Ogre::Vector2& nlb, Ogre::Vector2& nrb);
	virtual void onIntersect(GameObject* other, Timer* timer);
	virtual bool isOutlimit() const;
};
#endif
