#ifndef __GAMEFACTORY_H__
#define __GAMEFACTORY_H__

#ifndef _Ogre_H__
#include <Ogre.h>
#endif

class GameObject;
class GameObjectXML;

class Shot;

class GameFactory : public Ogre::Singleton<GameFactory>{

private:
	
	Ogre::SceneManager* _sceneManager;

protected:
	GameFactory();
	~GameFactory();
public:
	int _config;
	
	static GameFactory& getSingleton();
	static GameFactory* getSingletonPtr();
	GameObject* createObjectGame (Ogre::String type, Ogre::String s);
	Shot* createShot (int);
	GameObject* createObjectGame (GameObjectXML* gameObjectXML);
	GameObject* createObjectGame (int type);
	GameObject* createObjectEmpty();
	void setConfig(int config) {_config = config;};
};

#endif
