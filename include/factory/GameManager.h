#ifndef __GameManager_H__
#define __GameManager_H__

//#include "GameListener.h"
//#include "GameInput.h"
class GameListener;
class GameInput;

#ifndef __RenderWindow_H__  
#include <OgreRenderWindow.h>
#endif

#ifndef _Ogre_H__
#include <Ogre.h>
#endif

#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#include "sound/TrackManager.h"
#include "sound/SoundFXManager.h"
#include "factory/HighScores.h"
#include "factory/SaveGame.h"
#include <map>

class GameFactory;
class GameState;
class MainState;
class GameObject;


class GameManager : public Ogre::WindowEventListener {
	
private:
	static GameManager* pinstance;
	Ogre::SceneManager* _sceneManager;
	GameListener *_framelistener;
	GameInput *_gameInput;
	GameState* _gameState;
	MainState* _mainState;
	HighScores* _pHighScores;
	TrackManager* _pTrackManager;
	SoundFXManager* _pSoundFXManager;
	TrackPtr _mainTrack;
	SoundFXPtr _simpleEffect;
	SoundFXPtr _simpleEffect2;
	SaveGame* _saveGame;
	Ogre::Camera* _camera;
	GameFactory* _gameFactory;
	


protected:
	GameManager();
	~GameManager();
	
public:
	/*Var public */
	float _width;
	float _height;

	//JRC
	std::map<std::string, TrackPtr> mTracksmap;
	std::map<std::string, SoundFXPtr> mSoundsFXmap;

	/*Func*/
	static GameManager* getSingleton();
	void initialize(Ogre::RenderWindow *window);
	void loadCEGUI();
	bool initSDL ();
	void loadResources();
	void pause();
	void end();
	void windowResized(Ogre::RenderWindow* rw);
	void windowClosed(Ogre::RenderWindow* rw);
	void addTimer(Ogre::String timer);
	void addTimer(unsigned long timer);
	GameListener* getGameListener() const{return _framelistener;};
	GameInput* getGameInput() const{return _gameInput;};
	Ogre::SceneManager* getSceneManager () const{return _sceneManager;};
	GameFactory* getGameFactory() const {return _gameFactory;};
	GameState* getGameState() const{return _gameState;};
	MainState* getMainState() const{return _mainState;};
	HighScores* getHighScores() const{ return _pHighScores;};
	SaveGame* getSaveGame() const{ return _saveGame;};
	Ogre::Camera* getCamera() const {return _camera;};
	void saveGame(int level,int pts);

	void loadSounds();
	void playFX(Ogre::String);
	void unloadMainTrack();
	void playMainTrack(Ogre::String);

	void loadVarGlobal();
	void removeAll();
	
};

#endif
