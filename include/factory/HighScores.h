#ifndef _HIGHSCORES_H_
#define _HIGHSCORES_H_

#include <fstream>
#include <iostream>
#include <string>
#include <Ogre.h>
#include <string.h>


using namespace std;

class HighScores : public Ogre::Singleton<HighScores>{
private:
	int _nScores;
	list<string> _scores;
	
public:
	HighScores();
	~HighScores();

	static HighScores& getSingleton();
	static HighScores* getSingletonPtr();
	
	static bool compareScores(const string &a, const string &b);

	list<string> getScoresList() const{ return _scores; };
	void add(const char * name, int score);
	void write();
	void read();
};

#endif
