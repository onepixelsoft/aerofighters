#ifndef _SAVEGAME_H_
#define _SAVEGAME_H_

#include <fstream>
#include <iostream>
#include <string>
#include <Ogre.h>
#include <string.h>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace std;

class SaveGame : public Ogre::Singleton<SaveGame>{
private:
	
	
public:
	int _level;
	int _pts;
	SaveGame();
	~SaveGame();

	static SaveGame& getSingleton();
	static SaveGame* getSingletonPtr();
	
	

	int getLevel() const{ return _level; };
	void add(int level,int pts);
	void write();
	void read();
};

#endif
