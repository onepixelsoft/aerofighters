#ifndef __AIBEHAVIOUR__
#define __AIBEHAVIOUR__

#include <Ogre.h>
#include "factory/GameManager.h"
#include "game/GameState.h"
#include "game/Enemy.h"


class GameObject;
class Timer;

class AIBehaviour {
 public:
	 int _health_max;
	 int _velocity;
  Enemy* _parent;
	AIBehaviour (GameObject* player) { _player = player; }

  
  virtual void hit (GameObject* gameObject, Timer* timer) = 0;
  virtual void update (const Ogre::FrameEvent & evt, Timer* timer) = 0;
  virtual void remove()
  {
	 _parent->destroy();
  }


 protected:
  GameObject* _player;
  
}; 

class EasyAIBehaviour: public AIBehaviour {
 public:
  EasyAIBehaviour (GameObject* player): AIBehaviour(player) {_health_max = 0;}
  void hit (GameObject* gameObject, Timer* timer);
   void update (const Ogre::FrameEvent & evt, Timer* timer);
   
};

class NormalAIBehaviour: public AIBehaviour {
 public:
	
  NormalAIBehaviour (GameObject* player): AIBehaviour(player) {}
  void hit (GameObject* gameObject, Timer* timer);
  void update (const Ogre::FrameEvent & evt, Timer* timer);
   
};

class HardAIBehaviour: public AIBehaviour {
 public:
  HardAIBehaviour (GameObject* player) : AIBehaviour(player) {}
  void hit (GameObject* gameObject, Timer* timer);
  void update (const Ogre::FrameEvent & evt, Timer* timer);
  
};

class ExtreamAIBehaviour: public AIBehaviour {
 public:
  ExtreamAIBehaviour (GameObject* player) : AIBehaviour(player) {}
  void hit (GameObject* gameObject, Timer* timer);
  void update (const Ogre::FrameEvent & evt, Timer* timer);
   
};

#endif