#ifndef __Asteroid_H__
#define __Asteroid_H__


#include <Ogre.h>
#include "game/Enemy.h"


class Asteroid : public Enemy
{
private:
 

 
public:
	
	Asteroid(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	Asteroid(const Asteroid &obj);
	~Asteroid();

	void update(const Ogre::FrameEvent& evt, Timer* timer);
	void onIntersect(GameObject* gameObject, Timer* timer);
	void destroy();
	void hit(GameObject* gameObject, int damage);
	void move(float x, float y);
	//<JRC
	Ogre::Vector3 mRotVec;

};


#endif
