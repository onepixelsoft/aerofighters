#ifndef __Enemy_H__
#define __Enemy_H__


#include <Ogre.h>
#include "engine/GameObject.h"

class AIBehaviour;

class Enemy : public GameObject
{
private:
  short _state;
  bool _up;
  bool _down;
  bool _rigth;
  bool _left;
  
 
public:
	////<JRC:puntero al sistema de particulas del propulsor
	Ogre::ParticleSystem* m_pTrailPart;	
	int _score;
	 short _health;
	 short _maxHealth;
	 int _velocity;
	 short _category;
	 short _damage;
	 AIBehaviour* _ai;
	Enemy(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	Enemy(const Enemy &card);
	~Enemy();
	void onclick();
	void mouseIn();
	void mouseOut();
	virtual void update(const Ogre::FrameEvent& evt, Timer* timer);
	void moveUp();
	void moveDown();
	void moveRigth();
	void moveLeft();
	virtual void onIntersect(GameObject* gameObject, Timer* timer);
	virtual void destroy();
	virtual void hit(GameObject* gameObject, int damage);
	virtual void move(float x, float y);
	virtual void shoot(unsigned long timer);
};


#endif
