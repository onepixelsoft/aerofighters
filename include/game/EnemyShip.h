#ifndef __EnemyShip_H__
#define __EnemyShip_H__


#include <Ogre.h>
#include "game/Enemy.h"


class Weapon;

class EnemyShip : public Enemy
{
private:
 
	short _direction;
		
public:
	Weapon* _weapon;
	

	EnemyShip(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	EnemyShip(const EnemyShip &obj);
	~EnemyShip();
	
	void update(const Ogre::FrameEvent& evt, Timer* timer);
	void onIntersect(GameObject* gameObject, Timer* timer);
	void destroy();
	void hit(GameObject* gameObject, int damage);
	void move(float x, float y);
	void shoot(unsigned long timer);
};


#endif
