#ifndef __EXPLOSION__H
#define __EXPLOSION__H

#include "Ogre.h"
#include "tools/Timer.h"
#include "engine/GameObject.h"

class Explosion : GameObject{
public:
	Explosion();
	Explosion( Ogre::SceneManager*, Ogre::Vector3, float ,  const Ogre::String&, const Ogre::String&);
	Explosion(const Explosion&);
	~Explosion();
	
	void destroy();
	void update(const Ogre::FrameEvent&, Timer*);

private:
	static int mID;
	float mStartTime, mLifeTime;
	Ogre::ParticleSystem* m_pParticle;
	Ogre::SceneManager* m_pSceneMgr;

	

};

#endif 
