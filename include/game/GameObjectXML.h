#ifndef __GOXML_H__
#define __GOXML_H__

#include <Ogre.h>
#include <vector>

class Pts;

class GameObjectXML 
{
	
public:
	bool _build;
	int _type;
	float _x;
	float _y;
	float _z;
	int _category;
	int _move_ai;
	int _shot_ai;
	int _velocity;
	int _weapon;
	int _number;
	int _remove;
	int _explosion;
	int _x_low;
	int _x_high;
	int _y_low;
	int _y_high;
	int _wait;
	GameObjectXML();
	GameObjectXML(const GameObjectXML &card);
	~GameObjectXML();
	
};


#endif