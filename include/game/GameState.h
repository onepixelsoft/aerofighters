#ifndef __GameState_H__
#define __GameState_H__

#include <Ogre.h>
#include <OIS/OIS.h>
#include <vector>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>


class GameObject;
class Enemy;
class Pts;
class GameXML;
class Level;
class Pts;
class Timer;

class GameState {

private:
	short _state;
	unsigned long _timer;
	int _score;
	short _config;
	GameObject* player1;
	GameObject* player2;
	GameXML* _gameXML;
	int _nLevel;
	int _nPts;
	Level* _level;
	Pts* _pts;
	bool _first;
	int _lengthGos;

	int EndingTime;
	int waitTillEnding;
public:
	bool win;
	std::vector<GameObject*> _players;
	Ogre::SceneNode* _scene;
	Ogre::SceneNode* _sceneLayer;
	GameState();
	~GameState();
	void init();
	void begin_update(const Ogre::FrameEvent& evt, Timer* timer);
	void end_update(const Ogre::FrameEvent& evt, Timer* timer);
	void setState(short state);
	void setConfig(short config);
	void scoreDecrease();
	void scoreAdd(int);
	void reset();
	short getState() const {return _state;};
	int getScore(){return _score;};
	short getConfig() const{return _config;};
	void keyboard(OIS::Keyboard* keyboard, Timer* timer);
	void keyboard (const OIS::KeyEvent & evt, Timer* timer);
	void addWorld(GameObject* gameObject);
	void removeWorld(GameObject* gameObject);
	void createWorldLevel ();
	void removeWorldLevel (Level* level);
	void setGameXML(GameXML* gameXML);
	void setLevel(int);
	void setPts(int);
	GameXML* getGameXML() const {return _gameXML;};
	void loadLevel(int time_wait);
	void uploadLevel();
	void nextPts();
	void nextLevel();
	void backPts();
	bool isNextPts();
	void removePtsGo();
	void endgame();
	void endLevel();
	void restart();
	void viewWaitsLevel();
	//<JRCHUD
	void createHUD();
	void updateHUD();
	void backmenu();
	void restartPlayer();
};

#endif
