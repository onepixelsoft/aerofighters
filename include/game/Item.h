#ifndef __Item_H__
#define __Item_H__


#include <Ogre.h>
#include "engine/GameObject.h"

class Player;

class Item : public GameObject
{
private:
  
  
 
public:
	
	short _velocity;
	short _category;
	unsigned long _time_init;
	int _time_use;
	bool _inUse;
	Player* _player;
	Item(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	Item(const Item &card);
	~Item();
	virtual void update(const Ogre::FrameEvent& evt, Timer* timer);
	virtual void onIntersect(GameObject* gameObject, Timer* timer);
};


#endif
