#ifndef __Level_H__
#define __Level_H__

#include <Ogre.h>
#include <vector>

class Pts;

class Level 
{
private:
	int _id;
	int _nextlevel;
public:
	std::vector<Pts*> _pts;
	Level();
	Level(const Level &card);
	~Level();
	void init();
	void update(const Ogre::FrameEvent& evt);
	void setId(int);
	int getId() const {return _id;};
	void setNextlevel(int);
	int getNextlevel() const {return _nextlevel;};
};


#endif