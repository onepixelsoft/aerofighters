#ifndef __MainState_H__
#define __MainState_H__

#include <Ogre.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

class GameState;

class MainState {

private:
	short _state;
	short _config;
	GameState* _gameState;

	CEGUI::Window* _sheetInit;
	CEGUI::Window* _sheetCredits;
	//<JR6:	
	//GUI sheet para mostrar el rankin de puntuaciones
	CEGUI::Window* _sheetHighScores; 
	//GUI sheet para insertar nombre y registrar puntuacion (aparece al terminar partida)
	CEGUI::Window* _sheetEndGameWin;
	CEGUI::Window* _sheetEndGameLose;

	CEGUI::Window* _sheetConfig;
	CEGUI::Window* _sheetGame;
	//CEGUI::Window* _sheetEnd;
	CEGUI::Window* _sheetPause;
public:
	MainState();
	~MainState();
	void init();
	void begin_update(const Ogre::FrameEvent& evt);
	void end_update(const Ogre::FrameEvent& evt);
	void setState(short state);
	void setConfig(short config);
	short getState() const {return _state;};
	short getConfig() const{return _config;};

	void loadCEGUI();

	void createSceneInit();
	void createSceneOptions();
	void createSceneCredits();
	void createSceneHighScores();
	void createSceneEndWin();
	void createSceneEndLose();
	void createSceneNext();
	void createScenePause();

	void pause();
	void end();
	void endlevel();

	bool backmenu(const CEGUI::EventArgs &e);	
	bool visibleSceneOptions(const CEGUI::EventArgs &e);
	bool loadGame(const CEGUI::EventArgs &e);	
	bool viewScores(const CEGUI::EventArgs &e);
	bool viewCredits(const CEGUI::EventArgs &e);
	bool submitScore(const CEGUI::EventArgs &e);
	bool restart(const CEGUI::EventArgs &e);
	bool resume(const CEGUI::EventArgs &e);
	bool back(const CEGUI::EventArgs &e);
	bool quit(const CEGUI::EventArgs &e);	
	void visibleSceneCredits();
	bool clickInit(const CEGUI::EventArgs &e);		
	bool createSceneGame(const CEGUI::EventArgs &e);
	
	
};

#endif
