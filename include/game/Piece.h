#ifndef __Piece_H__
#define __Piece_H__


#include <Ogre.h>
#include "engine/GameObject.h"



class Piece : public GameObject
{
private:
  
  
 
public:
	int _score;
	Ogre::Vector2* _direction;
	int _damage;
	Piece(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	Piece(const Piece &card);
	~Piece();
	virtual void update(const Ogre::FrameEvent& evt, Timer* timer);
	virtual void onIntersect(GameObject* gameObject, Timer* timer);
};


#endif
