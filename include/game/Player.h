#ifndef __Player_H__
#define __Player_H__


#include <Ogre.h>
#include "engine/GameObject.h"

class Weapon;

class Player : public GameObject
{
private:
  short _state;
  bool _up;
  bool _down;
  bool _rigth;
  bool _left;

//<JRC
	//puntero al sistema de particulas del propulsor
	Ogre::ParticleSystem* m_pTrailParticle;		
//>
	// TCR ----- Punteros a las animaciones de la nave
  Ogre::AnimationState *_animState, *_animState2, *_animState3, *_animState4;
  
  
  
public:

	int _health;
	int _maxHealth;
	Weapon* _weapon;

	Player(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	Player(const Player &card);
	~Player();
	void onclick();
	void mouseIn();
	void mouseOut();
	void update(const Ogre::FrameEvent& evt, Timer* timer);
	void moveUp();
	void moveDown();
	void moveRigth();
	void moveLeft();
	void onIntersect(GameObject* gameObject, Timer* timer);
	void respawn();
};


#endif
