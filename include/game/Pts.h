#ifndef __PTS_H__
#define __PTS_H__

#include <Ogre.h>
#include <vector>
#include "engine/GameObject.h"
#include "game/GameObjectXML.h"

class Level;

class Pts //Punto de guardado
{
private:
	
	int _id;
	int _nextpts;
	
	

public:
	unsigned long _time_init;
	int _lengthGos;
	std::vector<GameObjectXML*> _gosXml;
	Level* _parent;

	Pts();
	Pts(const Pts &card);
	~Pts();
	void init();
	void update(const Ogre::FrameEvent& evt);
	void setId(int);
	int getId() const {return _id;};
	void setNextpts(int);
	int getNextpts() const {return _nextpts;};
	void restart();
};


#endif