#ifndef __SHOT_H__
#define __SHOT_H__


#include <Ogre.h>
#include "engine/GameObject.h"



class Shot : public GameObject
{
private:
  
public:
	
	short _damage;
	short _inverter;
	Shot(Ogre::SceneManager* sceneManager, const Ogre::String& name);
	Shot(const Shot &card);
	~Shot();
	void update(const Ogre::FrameEvent& evt, Timer* timer);
	void onIntersect(GameObject* gameObject, Timer* timer);
};


#endif
