#ifndef __WEAPON_H__
#define __WEAPON_H__



#include "engine/GameObject.h"
#include "game/Shot.h"
#include <vector>



class Weapon 
{
private:
  
  short _type;
  std::vector<Shot*> _shots;
  int _wait;

 
  
public:
	int _tShoot;
	short _health;
	short _category;
	short _inverter;
	short _shotType;
	GameObject* _parent;

	Weapon();
	Weapon(const Shot &card);
	~Weapon();
	void shoot(unsigned long timer);
	
};


#endif
