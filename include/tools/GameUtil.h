#ifndef __GameUtil_H__
#define __GameUtil_H__

#ifndef _Ogre_H__
#include <Ogre.h>
#endif

#include <stdint.h> 
#include <OIS/OIS.h>
#include <CEGUI.h>  
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

class GameUtil : public Ogre::Singleton<GameUtil>{

private:
	
	Ogre::RaySceneQuery* _mRaySceneQuery;
	Ogre::SceneManager* _mSceneManager;
	
protected:
	GameUtil();
	~GameUtil();
public:
	static GameUtil& getSingleton();
	static GameUtil* getSingletonPtr();
	Ogre::MovableObject* getObjPoint(const OIS::MouseState &arg, CEGUI::Point mousePos, uint32_t mask);
	bool intersection(Ogre::SceneNode* _box_a, Ogre::SceneNode* box_b);
	bool getScreenspaceCoords(Ogre::MovableObject* object, Ogre::Camera* camera, Ogre::Vector2& result);
	
};

#endif
