/* **********************************************************
** Importador NoEscape 1.0
** Curso de Experto en Desarrollo de Videojuegos 
** Escuela Superior de Informatica - Univ. Castilla-La Mancha
** Carlos Gonzalez Morcillo - David Vallejo Fernandez
************************************************************/

#ifndef __IMPORTER_H__
#define __IMPORTER_H__

#include <Ogre.h>
#include <xercesc/dom/DOM.hpp>

class Level;
class Pts;
class GameXML;
class GameFactory;

class Importer: public Ogre::Singleton<Importer> {
 public:

	
  void parseGame (const char* path, GameXML* gameXML);
  static Importer& getSingleton ();
  static Importer* getSingletonPtr ();

private:
  void parseLevel (xercesc::DOMNode* levelNode, GameXML* gameXML);
	void parsePts (xercesc::DOMNode* ptsNode, Level* level);
	void parseGO (xercesc::DOMNode* goNode, Pts* pts);
	

};

#endif
