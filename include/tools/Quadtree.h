#ifndef __QUADTREE_H__
#define __QUADTREE_H__

#include <vector>
#include <Ogre.h>

using namespace std;

class Quadtree;
class GameObject;

class Quadtree {
public:
							Quadtree( float x, float y, float width, float height, int level, int maxLevel );
							~Quadtree();

	void					_AddObject( GameObject *object, Ogre::Vector2* nlt, Ogre::Vector2* nrt, Ogre::Vector2* nlb, Ogre::Vector2* nrb );
	void					_RemoveObject( GameObject *object, Ogre::Vector2* nlt, Ogre::Vector2* nrt, Ogre::Vector2* nlb, Ogre::Vector2* nrb );
	void					AddObject( GameObject *object );
	void					RemoveObject( GameObject *object );
	vector<GameObject*>			GetObjectsAt( GameObject* gameObject );
	void					Clear();
	bool					isEnterE(Ogre::Vector2* pos);
	bool					isEnterW(Ogre::Vector2* pos);
	bool					isEnterN(Ogre::Vector2* pos);
	bool					isEnterS(Ogre::Vector2* pos);
	bool					isContains(Quadtree *child, Ogre::Vector2* pos);

private:
	float					x;
	float					y;
	float					width;
	float					height;
	int						level;
	int						maxLevel;
	vector<GameObject*>			objects;

	Quadtree *				parent;
	Quadtree *				NW;
	Quadtree *				NE;
	Quadtree *				SW;
	Quadtree *				SE;

	bool					Contains( Quadtree *child, GameObject *object, Ogre::Vector2* nlt, Ogre::Vector2* nrt, Ogre::Vector2* nlb, Ogre::Vector2* nrb );
};


#endif
