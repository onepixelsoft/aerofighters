#ifndef __Timer_H__
#define __Timer_H__

#ifndef _Ogre_H__
#include <Ogre.h>
#endif
#include <stdio.h>
#include <iostream>
#include <string>


class Timer : public Ogre::Singleton<Timer>{

private:
	
	
	unsigned long _t;
	unsigned long _pause;

protected:
	Timer();
	~Timer();
public:
	Ogre::Timer* _timer;
	static Timer& getSingleton();
	static Timer* getSingletonPtr();
	std::string now_str();
	Ogre::String getTimerStr();
	void pause();
	void resume();
	void reset();
	unsigned long getCurrentTimer() const;
};

#endif
