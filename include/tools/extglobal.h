extern bool EXIT;
extern short STATE;
extern int X_MIN;
extern int X_MAX;
extern int Y_MIN;
extern int Y_MAX;

#define EASY 1
#define NORMAL 2
#define HARD 3
#define EXTREAM 4

#define INIT 0
#define INGAME 1
#define CREDITS 2
#define CONFIG 3
#define INPAUSE 4
#define END 5
#define ANIM 6
#define REMOVEALL 7
#define ENDLEVEL 8
#define WAITTOEND -1

#define GO 1 << 0
#define RANDOM -1
#define ENDLEVEL -1
#define PLAYER 1
#define ENEMY 2
#define PIECE 3
#define ITEM 4
#define WEAPON 40
#define SHOT 50

#define ENEMYSHIP  2
#define BOSS  3
