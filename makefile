# --------------------------------------------------------------------
# Makefile Genérico :: Módulo 2. Curso Experto Desarrollo Videojuegos
# Carlos González Morcillo     Escuela Superior de Informática (UCLM)
# --------------------------------------------------------------------
EXEC := main

DIRSRC := src/
DIROBJ := obj/
DIRHEA := include/
DIRCEGUI := /usr/local/include/CEGUI/
DIROGRE := /usr/include/OGRE/
DIRSDL := SDL/

SRCTOOLS := tools/
SRCSOUND := sound/
SRCGAME := game/
SRCFACT := factory/
SRCENGINE := engine/
SRCAPP := app/


CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := -I $(DIRHEA) -I $(DIRHEA)$(DIRSDL) -I $(DIRCEGUI) -I $(DIRCEGUI)$(DIROGRE) -Wall `pkg-config --cflags OGRE --libs sdl` 

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs-only-L OGRE --libs sdl`
LDLIBS := `pkg-config --libs-only-l OGRE --libs sdl` -lOIS -lGL -lstdc++ -lCEGUIBase -lCEGUIOgreRenderer -lCEGUIFalagardWRBase -lOgreMain -lSDL_image -lSDL_mixer -lboost_system -L/usr/local/lib -lxerces-c

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release) 
	CXXFLAGS += -O2 -D_RELEASE
else 
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(subst $(DIRSRC)$(SRCTOOLS), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)$(SRCTOOLS)*.cpp))) \
	$(subst $(DIRSRC)$(SRCSOUND), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)$(SRCSOUND)*.cpp))) \
	$(subst $(DIRSRC)$(SRCGAME), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)$(SRCGAME)*.cpp))) \
	$(subst $(DIRSRC)$(SRCFACT), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)$(SRCFACT)*.cpp))) \
	$(subst $(DIRSRC)$(SRCENGINE), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)$(SRCENGINE)*.cpp))) \
	$(subst $(DIRSRC)$(SRCAPP), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)$(SRCAPP)*.cpp)))
	

.PHONY: all clean

all: info compile $(EXEC)

info:
	@echo '------------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])  '
	@echo '------------------------------------------------------'
	echo $(OBJS)

# Enlazado -----------------------------------------------------------
compile:
	+$(MAKE) -C src/ -f maketools
	+$(MAKE) -C src/ -f makesound
	+$(MAKE) -C src/ -f makegame
	+$(MAKE) -C src/ -f makefactory
	+$(MAKE) -C src/ -f makeengine
	+$(MAKE) -C src/ -f makeapp


$(EXEC): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# Compilación --------------------------------------------------------
#$(DIROBJ)%.o: $(DIRSRC)%.cpp
#	$(CXX) $(CXXFLAGS) -o $@  $(LDLIBS) "-c $<"
#	
#
# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(DIROBJ)*.o $(DIRSRC)*~ $(DIRHEA)*~ $(DIRSRC)*/*~ savegame

edit:
	emacs $(wildcard $(DIRSRC)*.cpp) $(wildcard $(DIRHEA)*.h) &
