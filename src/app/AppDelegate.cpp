#include "app/AppDelegate.h"

AppDelegate::AppDelegate ()
{

}

AppDelegate::~AppDelegate ()
{
  //delete _root;

}

int
AppDelegate::start ()
{
  _root = new Ogre::Root ();

  if (!_root->restoreConfig ())
    {
      _root->showConfigDialog ();
      _root->saveConfig ();
    }

  Ogre::RenderWindow * window = _root->initialise (true, "TuxMemory");

  _gameManager = GameManager::getSingleton ();
  _gameManager->initialize (window);

  _root->startRendering ();
  return 0;
}
