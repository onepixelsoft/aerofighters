#include "engine/GameInput.h"
#include "factory/GameManager.h"
#include "tools/extglobal.h"
#include "engine/GameListener.h"
#include "engine/GameObject.h"
#include "game/MainState.h"
#include "tools/Timer.h"

class GameState;

GameInput::GameInput (Ogre::RenderWindow * win,
		      Ogre::SceneManager * sceneManager,
		      GameManager * gameManager)
{
  _inObj = 0;
  _gameManager = gameManager;
  _sceneManager = sceneManager;
  _win = win;
  OIS::ParamList param;
  size_t windowHandle;
  std::ostringstream wHandleStr;

  win->getCustomAttribute ("WINDOW", &windowHandle);
  wHandleStr << windowHandle;
  param.insert (std::make_pair ("WINDOW", wHandleStr.str ()));

  param.insert (std::make_pair (std::string ("WINDOW"), wHandleStr.str ()));
  /* Quitado, ya lo controla CEGUI
     #if defined OIS_WIN32_PLATFORM

     param.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
     param.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
     param.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
     param.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
     #elif defined OIS_LINUX_PLATFORM
     param.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("true")));
     param.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
     param.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
     param.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
     #endif
   */
  _inputManager = OIS::InputManager::createInputSystem (param);
  _keyboard = static_cast < OIS::Keyboard * >
    (_inputManager->createInputObject (OIS::OISKeyboard, true));
  _mouse = static_cast < OIS::Mouse * >
    (_inputManager->createInputObject (OIS::OISMouse, true));

  _mouse->setEventCallback (this);
  _keyboard->setEventCallback (this);

}

GameInput::~GameInput ()
{
  _inputManager->destroyInputObject (_keyboard);
  OIS::InputManager::destroyInputSystem (_inputManager);

}

void
GameInput::setGameListener (GameListener * gameListener)
{
  _gameListener = gameListener;
}

void GameInput::read (const Ogre::FrameEvent & evt, Timer* timer)
{
	_timer = timer;
  _keyboard->capture ();
  _mouse->capture ();
  CEGUI::System::getSingleton ().injectTimePulse (evt.timeSinceLastFrame);
  _gameManager->getGameState()->keyboard(_keyboard, _timer);
}


OIS::Mouse * GameInput::getOisMouse () const
{
  return _mouse;
}


bool GameInput::keyPressed (const OIS::KeyEvent & evt)
{
  CEGUI::System::getSingleton ().injectKeyDown (evt.key);
  CEGUI::System::getSingleton ().injectChar (evt.text);
  if (evt.key == OIS::KC_ESCAPE)
    {
      _gameManager->getMainState()->pause ();


    }

  _gameManager->getGameState()->keyboard(evt, _timer);

  return true;
}

bool GameInput::keyReleased (const OIS::KeyEvent & evt)
{
  CEGUI::System::getSingleton ().injectKeyUp (evt.key);
  
  return true;
}

bool GameInput::mousePressed (const OIS::MouseEvent & evt, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton ().
    injectMouseButtonDown (convertMouseButton (id));
  CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton ().getPosition ();
  /*Ogre::MovableObject * inObj =
    GameUtil::getSingleton ().getObjPoint (_mouse->getMouseState (),
					    mousePos, GO);
  
  if (id == OIS::MB_Left
      && _gameManager->getGameState()->getState() == INGAME)
    {
      if (inObj)
	{
	  if (!_inObj)
	    {
	      _inObj = inObj;
	      GameObject *go = Ogre::any_cast <GameObject *>(_inObj->getParentNode ()->getUserObjectBindings ().getUserAny ("go"));
	      go->onclick ();
	    }
	  else
	    {
	      if (_inObj->getName () == inObj->getName ())
		{
		  GameObject *go =
		    Ogre::any_cast <
		    GameObject *
		    >(_inObj->getParentNode ()->getUserObjectBindings ().
		      getUserAny ("go"));
		  go->onclick ();
		}
	      else
		{
		  GameObject *goIn =
		    Ogre::any_cast <
		    GameObject *
		    >(_inObj->getParentNode ()->getUserObjectBindings ().
		      getUserAny ("go"));
		  goIn->mouseOut ();

		  _inObj = inObj;

		  GameObject *go =
		    Ogre::any_cast <
		    GameObject *
		    >(_inObj->getParentNode ()->getUserObjectBindings ().
		      getUserAny ("go"));
		  go->onclick ();
		}
	    }



	}
      else
	{
	  if (_inObj)
	    {
	      GameObject *goIn =
		Ogre::any_cast <
		GameObject *
		>(_inObj->getParentNode ()->getUserObjectBindings ().
		  getUserAny ("go"));
	      goIn->mouseOut ();
	    }
	  _inObj = 0;
	}
    }
	*/
  return true;
}

bool GameInput::mouseReleased (const OIS::MouseEvent & evt, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton ().
    injectMouseButtonUp (convertMouseButton (id));
  CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton ().getPosition ();
  /*Ogre::MovableObject * inObj =
    GameUtil::getSingleton ().getObjPoint (_mouse->getMouseState (),
					    mousePos, GO);
  
  if (id == OIS::MB_Left
      && _gameManager->getGameState ()->getState () == INGAME)
    {
      if (inObj)
	{
	  if (_inObj)
	    {
	      if (_inObj->getName () != inObj->getName ())
		{
		  GameObject *goIn =
		    Ogre::any_cast <
		    GameObject *
		    >(_inObj->getParentNode ()->getUserObjectBindings ().
		      getUserAny ("go"));
		  goIn->mouseOut ();

		  _inObj = inObj;
		}
	    }



	}
      else
	{
	  if (_inObj)
	    {
	      GameObject *goIn =
		Ogre::any_cast <
		GameObject *
		>(_inObj->getParentNode ()->getUserObjectBindings ().
		  getUserAny ("go"));
	      goIn->mouseOut ();
	    }
	  _inObj = 0;
	}
    }*/
  return true;
}

bool GameInput::mouseMoved (const OIS::MouseEvent & evt)
{
  CEGUI::System::getSingleton ().injectMouseMove (evt.state.X.rel,
						  evt.state.Y.rel);
  CEGUI::Point mousePos = CEGUI::MouseCursor::getSingleton ().getPosition ();
  /*Ogre::MovableObject * inObj =
    GameUtil::getSingleton ().getObjPoint (_mouse->getMouseState (),
					    mousePos, GO);
  
  if (inObj)
    {
      if (!_inObj)
	{
	  _inObj = inObj;
	  GameObject *go =
	    Ogre::any_cast <
	    GameObject *
	    >(_inObj->getParentNode ()->getUserObjectBindings ().
	      getUserAny ("go"));
	  go->mouseIn ();
	}
      else
	{
	  if (_inObj->getName () == inObj->getName ())
	    {
	      GameObject *go =
		Ogre::any_cast <
		GameObject *
		>(_inObj->getParentNode ()->getUserObjectBindings ().
		  getUserAny ("go"));
	      go->mouseOver ();
	    }
	  else
	    {
	      GameObject *goIn =
		Ogre::any_cast <
		GameObject *
		>(_inObj->getParentNode ()->getUserObjectBindings ().
		  getUserAny ("go"));
	      goIn->mouseOut ();

	      _inObj = inObj;

	      GameObject *go =
		Ogre::any_cast <
		GameObject *
		>(_inObj->getParentNode ()->getUserObjectBindings ().
		  getUserAny ("go"));
	      go->mouseIn ();
	    }
	}



    }
  else
    {
      if (_inObj)
	{
	  GameObject *goIn =
	    Ogre::any_cast <
	    GameObject *
	    >(_inObj->getParentNode ()->getUserObjectBindings ().
	      getUserAny ("go"));
	  goIn->mouseOut ();
	}
      _inObj = 0;
    }
	*/
  return true;
}

CEGUI::MouseButton GameInput::convertMouseButton (OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch (id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}


