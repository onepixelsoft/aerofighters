/*********************************************************************
 * Módulo 2. Curso de Experto en Desarrollo de Videojuegos
 * Autor: Carlos González Morcillo     Carlos.Gonzalez@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/
#include "engine/GameListener.h"
#include "factory/GameManager.h"
#include "tools/extglobal.h"
#include "engine/GameInput.h"
#include "engine/GameObject.h"
#include "game/GameState.h"
#include "tools/Quadtree.h"
#include "tools/GameUtil.h"
#include <vector>
#include "tools/Timer.h"

GameListener::GameListener (Ogre::RenderWindow * win,
			    Ogre::SceneManager * sceneManager)
{

  _sceneManager = sceneManager;
  _quadtree = new Quadtree(0,0,1, 1, 1, 3);
  _timer = Timer::getSingletonPtr();
  _removeAll = false;
  _scene = sceneManager->createSceneNode ();
  _layerScene = sceneManager->createSceneNode ();
  _sceneManager->getRootSceneNode ()->addChild (_scene);
  _sceneManager->getRootSceneNode ()->addChild (_layerScene);
}

GameListener::~GameListener ()
{
	delete _quadtree;
}

bool
GameListener::frameRenderingQueued (const Ogre::FrameEvent & evt)
{
  _gameInput->read (evt,_timer);
  return true;
}


bool
GameListener::frameStarted (const Ogre::FrameEvent & evt)
{
  _gameState->begin_update (evt, _timer);
  Ogre::Node::ChildNodeIterator itr =
  _scene->getChildIterator ();

	//FINAL
  while (itr.hasMoreElements () && ( (_gameState->getState() == INGAME)||(_gameState->getState() == WAITTOEND) ) )
  {
	
	Ogre::SceneNode * child = (Ogre::SceneNode *) itr.getNext ();

	if (child) {
		  GameObject *go =
		Ogre::any_cast <
		GameObject * >(child->getUserObjectBindings ().getUserAny ("go"));
	
	    
		  if (go->_isUpdate && !go->_remove) {
			 if (go->_update_quadtree)
				_quadtree->RemoveObject(go);
			  go->update (evt, _timer);
			  if (go->_update_quadtree)
				_quadtree->AddObject(go);
			
			  if (go->_isIntersect && !go->_remove) {
					std::vector<GameObject*> finds = _quadtree->GetObjectsAt(go);
		
		
					for (std::vector<GameObject*>::iterator p = finds.begin(); p != finds.end(); p++) 
					{
						GameObject* find = static_cast<GameObject*>(*p);
						if (!go->_remove && find != NULL && !find->_remove && find->_isIntersect && GameUtil::getSingleton().intersection(go->getSceneNode(), find->getSceneNode()) && find->_type != go->_type && !((find->_type == ITEM && go->_type == ENEMY) || (find->_type == ENEMY && go->_type == ITEM)))
						{
							go->onIntersect(find, _timer);
							find->onIntersect(go, _timer);
						
						} 
					}

				
			  }
		  }

		  if (go->_remove || _removeAll) {
			  if (go->_update_quadtree)
				_quadtree->RemoveObject(go);
			  _gameState->removeWorld(go);
			  delete go;
		  }
	}
    }
   _gameState->end_update (evt, _timer);
	
  return EXIT;
}






void
GameListener::setGameInput (GameInput * gameInput)
{
  _gameInput = gameInput;
  _gameState = gameInput->getGameManager()->getGameState ();
}

void GameListener::removeAll()
{
	 Ogre::Node::ChildNodeIterator itr =
  _scene->getChildIterator ();

  while (itr.hasMoreElements ())
  {
	
	Ogre::SceneNode * child = (Ogre::SceneNode *) itr.getNext ();

	if (child) {
		  GameObject *go =
		Ogre::any_cast <
		GameObject * >(child->getUserObjectBindings ().getUserAny ("go"));
	
	    
		 
		  if (go->_removeAll) {
			  if (go->_update_quadtree)
				_quadtree->RemoveObject(go);
		 _scene->removeChild(go->getSceneNode());
		 delete go;
		  }
		  
	}
    }
}
