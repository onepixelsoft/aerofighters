#include "engine/GameObject.h"
#include "factory/GameManager.h"
#include "tools/extglobal.h"
#include "tools/Timer.h"

GameObject::GameObject ()
{

}

GameObject::GameObject (Ogre::SceneManager * sceneManager,
			const Ogre::String & name)
{
  _sceneNode = sceneManager->createSceneNode ();
  _isIntersect = true;
  _update_quadtree = true;
  if (name != "notent") {    
    _ent = sceneManager->createEntity (name+".mesh");
	_sceneNode->attachObject (_ent);
	 _ent->setQueryFlags(GO);

  } else {
	_ent = 0;
	_isIntersect = false;
	_update_quadtree = false;
  }
  _sceneNode->getUserObjectBindings ().setUserAny ("go", Ogre::Any (this));
  
  //_sceneNode->showBoundingBox(true);

  
  
  _isUpdate = true;
  _remove = false;
  _removeAll = true;
}

GameObject::~GameObject ()
{
	if(_ent)
		delete _ent;
	delete _sceneNode;
}

void GameObject::onclick ()
{
  //OL::OverlayLog::set("fpsInfo", "click sobre "+_ent->getName());
}

void GameObject::update (const Ogre::FrameEvent & evt, Timer* timer)
{

}

void GameObject::mouseIn ()
{
  //OL::OverlayLog::set("fpsInfo", "mouseIN sobre "+_ent->getName());
}

void GameObject::mouseOver ()
{
  //OL::OverlayLog::set("fpsInfo", "mouseOver sobre "+_ent->getName());
}

void GameObject::mouseOut ()
{
  //OL::OverlayLog::set("stringInfo", "mouseOut sobre "+_ent->getName());
}

void GameObject::keyboard (OIS::Keyboard* keyboard, Timer* timer)
{

}

float GameObject::getX() const
{
	return getBox().getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP).x;
}

float GameObject::getY() const
{
	return getBox().getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP).y;
}

float GameObject::getWidth() const
{
	return getBox().getSize().x;
}

float GameObject::getHeight() const
{
	return getBox().getSize().y;
}

bool GameObject::getPosCenter(Ogre::Vector2& result)
{
   const Ogre::AxisAlignedBox &AABB = _ent->getWorldBoundingBox(true);
 
   /**
   * If you need the point above the object instead of the center point:
   * This snippet derives the average point between the top-most corners of the bounding box
   * Ogre::Vector3 point = (AABB.getCorner(AxisAlignedBox::FAR_LEFT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::FAR_RIGHT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::NEAR_LEFT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::NEAR_RIGHT_TOP)) / 4;
   */

   Ogre::Camera* camera = GameManager::getSingleton()->getCamera();
 
   // Get the center point of the object's bounding box
   Ogre::Vector3 point = AABB.getCenter();
 
   // Is the camera facing that point? If not, return false
   Ogre::Plane cameraPlane = Ogre::Plane(Ogre::Vector3(camera->getDerivedOrientation().zAxis()), camera->getDerivedPosition());
   if(cameraPlane.getSide(point) != Ogre::Plane::NEGATIVE_SIDE)
      return false;
 
   // Transform the 3D point into screen space
   point = camera->getProjectionMatrix() * (camera->getViewMatrix() * point);
 
   // Transform from coordinate space [-1, 1] to [0, 1] and update in-value
   result.x = (point.x / 2) + 0.5f;
   result.y = 1 - ((point.y / 2) + 0.5f);
 
   return true;
}

bool GameObject::getPosAll(Ogre::Vector2& nlt, Ogre::Vector2& nrt, Ogre::Vector2& nlb, Ogre::Vector2& nrb)
{
   const Ogre::AxisAlignedBox &AABB = _ent->getWorldBoundingBox(true);
 
   /**
   * If you need the point above the object instead of the center point:
   * This snippet derives the average point between the top-most corners of the bounding box
   * Ogre::Vector3 point = (AABB.getCorner(AxisAlignedBox::FAR_LEFT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::FAR_RIGHT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::NEAR_LEFT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::NEAR_RIGHT_TOP)) / 4;
   */

   Ogre::Camera* camera = GameManager::getSingleton()->getCamera();
 
   // Get the center point of the object's bounding box
   Ogre::Vector3 point1 = AABB.getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_TOP);
   Ogre::Vector3 point2 = AABB.getCorner(Ogre::AxisAlignedBox::NEAR_RIGHT_TOP);
   Ogre::Vector3 point3 = AABB.getCorner(Ogre::AxisAlignedBox::NEAR_LEFT_BOTTOM);
   Ogre::Vector3 point4 = AABB.getCorner(Ogre::AxisAlignedBox::NEAR_RIGHT_BOTTOM);
 
   // Is the camera facing that point? If not, return false
   Ogre::Plane cameraPlane = Ogre::Plane(Ogre::Vector3(camera->getDerivedOrientation().zAxis()), camera->getDerivedPosition());
   if(cameraPlane.getSide(point1) != Ogre::Plane::NEGATIVE_SIDE)
      return false;
   if(cameraPlane.getSide(point2) != Ogre::Plane::NEGATIVE_SIDE)
      return false;
   if(cameraPlane.getSide(point3) != Ogre::Plane::NEGATIVE_SIDE)
      return false;
   if(cameraPlane.getSide(point4) != Ogre::Plane::NEGATIVE_SIDE)
      return false;
 
   // Transform the 3D point into screen space
   point1 = camera->getProjectionMatrix() * (camera->getViewMatrix() * point1);
   point2 = camera->getProjectionMatrix() * (camera->getViewMatrix() * point2);
    point3 = camera->getProjectionMatrix() * (camera->getViewMatrix() * point3);
	 point4 = camera->getProjectionMatrix() * (camera->getViewMatrix() * point4);
   // Transform from coordinate space [-1, 1] to [0, 1] and update in-value
   nlt.x = (point1.x / 2) + 0.5f;
   nlt.y = 1 - ((point1.y / 2) + 0.5f);

   nrt.x = (point2.x / 2) + 0.5f;
   nrt.y = 1 - ((point2.y / 2) + 0.5f);

   nlb.x = (point3.x / 2) + 0.5f;
   nlb.y = 1 - ((point3.y / 2) + 0.5f);

   nrb.x = (point4.x / 2) + 0.5f;
   nrb.y = 1 - ((point4.y / 2) + 0.5f);
 
   return true;
}

void GameObject::onIntersect(GameObject* other, Timer* timer)
{

}

bool GameObject::isOutlimit() const
{
	Ogre::Vector3 position = _sceneNode->getPosition();
	return  (position.x > X_MAX || position.x < X_MIN || position.y > Y_MAX || position.y < Y_MIN);
	
}
