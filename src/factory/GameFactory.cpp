#include "factory/GameFactory.h"
#include "factory/GameManager.h"
#include "game/GameState.h"
#include "engine/GameObject.h"
#include "game/Player.h"
#include "game/Enemy.h"
#include "game/Asteroid.h"
#include "game/GameObjectXML.h"

#include "tools/MyConfig.h"
#include "tools/extglobal.h"
#include "game/Weapon.h"
#include "game/Shot.h"
#include "game/Piece.h"
#include "game/Item.h"
#include "game/AIBehaviour.h"
#include "game/EnemyShip.h"

template <> GameFactory * Ogre::Singleton < GameFactory >::msSingleton = 0;



GameFactory::GameFactory ()
{
  
  Ogre::Root * root = Ogre::Root::getSingletonPtr ();
  _sceneManager = root->getSceneManager ("SceneManagerInstance1");
  _config = 1;
  
}
  


GameFactory::~GameFactory ()
{
}

GameFactory & 
	GameFactory::getSingleton ()
    
{
	if (!msSingleton)
		msSingleton = new GameFactory ();
	assert (msSingleton);

	return (*msSingleton);

}



GameFactory * 
	GameFactory::getSingletonPtr () 
{
	if (!msSingleton)
		msSingleton = new GameFactory ();
	assert (msSingleton);

	return msSingleton;

}

GameObject* GameFactory::createObjectGame (Ogre::String type, Ogre::String s)
{
  if (type == "player")
  {
	  Player* player = new Player(_sceneManager, s);
	  player->_weapon = new Weapon();
	  player->_weapon->_parent = player;
		//JRC		
		player->_weapon->_shotType = PLAYER;
	  player->_health =  MyConfig::getInstance().getValueAsInt("Player/health");
	  return player;
  } else if (type == "enemy1")
  {
	return new Enemy(_sceneManager, s);
  }
}

GameObject* GameFactory::createObjectGame (int type)
{
	if (type >= SHOT) 
	{

		Ogre::String type_s = Ogre::StringConverter::toString(type);
		Shot* shot = new Shot(_sceneManager, MyConfig::getInstance().getValueAsString("Items/Shot"+type_s));
		shot->_damage =  MyConfig::getInstance().getValueAsInt("Items/Shot"+type_s+"_damage");
		return shot;

	} else if(type == PIECE) 
	{
		Ogre::String type_s = Ogre::StringConverter::toString(type);
		Piece* piece = new Piece(_sceneManager, MyConfig::getInstance().getValueAsString("Items/Piece"));
		piece->_damage =  MyConfig::getInstance().getValueAsInt("Items/Piece_damage");
		int velocity = MyConfig::getInstance().getValueAsInt("Items/Piece_velocity");
		piece->_direction = new Ogre::Vector2(velocity*Ogre::Math::RangeRandom(-1, 1),velocity*Ogre::Math::RangeRandom(-1, 1));

		return piece;
	}
   
}

Shot* GameFactory::createShot (int type)
{
		//carga diferentes meshes para disparos del jugador y enemigos
		if(type == PLAYER){
			Shot* shot = new Shot(_sceneManager, MyConfig::getInstance().getValueAsString("Items/Shot50"));
			shot->_damage =  MyConfig::getInstance().getValueAsInt("Items/Shot50_damage");
			GameManager::getSingleton()->playFX("PlayerShot");				
			return shot;
		}
		else if(type == ENEMY){
			Ogre::String _confif_s = Ogre::StringConverter::toString(_config);
			Shot* shot = new Shot(_sceneManager, MyConfig::getInstance().getValueAsString("Items/EnemyShot50"));
			shot->_damage =  MyConfig::getInstance().getValueAsInt("Items/EnemyShot50_damage"+_confif_s);
			GameManager::getSingleton()->playFX("EnemyShot");		
			return shot;
		}

   
}

GameObject* GameFactory::createObjectEmpty ()
{
	return new GameObject(_sceneManager, "notent");
   
}

GameObject* GameFactory::createObjectGame (GameObjectXML* gameObjectXML)
{
	/*
	
	*/
	Ogre::String category_s = Ogre::StringConverter::toString(gameObjectXML->_category);
	Ogre::Real x = gameObjectXML->_x;
	Ogre::Real y = gameObjectXML->_y;
	Ogre::Real z = gameObjectXML->_z;
	if (x == RANDOM)
		x = Ogre::Math::RangeRandom(gameObjectXML->_x_low, gameObjectXML->_x_high);
	if (y == RANDOM)
		y = Ogre::Math::RangeRandom(gameObjectXML->_y_low, gameObjectXML->_y_high);
 if (gameObjectXML->_type == ENEMY)
  {
	  GameObject* player = GameManager::getSingleton()->getGameState()->_players[0];
	  Enemy* enemy = 0;
	  if (gameObjectXML->_category == 1) 
		enemy = new Asteroid(_sceneManager, MyConfig::getInstance().getValueAsString("Mesh/Rock"));
	  else if (gameObjectXML->_category == 2) {
		  enemy = new EnemyShip(_sceneManager, MyConfig::getInstance().getValueAsString("Mesh/EnemyShip"+category_s));
		  

	  }
	  else if (gameObjectXML->_category == 3) {
		  enemy = new EnemyShip(_sceneManager, MyConfig::getInstance().getValueAsString("Mesh/EnemyShip"+category_s));
			enemy->_category = BOSS;
			enemy->_score = MyConfig::getInstance().getValueAsInt("Enemy/boss_score");;
		  ((EnemyShip*)enemy)->_weapon->_category = 2;
 short _health;
			GameManager::getSingleton()->playMainTrack("POL-savage-match-short.wav");
	  }
	
	enemy->_type = gameObjectXML->_type;
	enemy->getSceneNode()->setPosition(x,y,z);
	enemy->_category = gameObjectXML->_category;
	enemy->_velocity = gameObjectXML->_velocity;
	enemy->_health =  MyConfig::getInstance().getValueAsInt("Enemy/health"+category_s);
	//enemy->_health =  MyConfig::getInstance().getValueAsInt("Enemy/health");

	switch (_config)
	{
	case EASY:
		{
		enemy->_ai = new EasyAIBehaviour(player);
		enemy->_ai->_health_max = MyConfig::getInstance().getValueAsInt("Enemy/normal_health");
		enemy->_ai->_velocity =  MyConfig::getInstance().getValueAsInt("Enemy/normal_velocity");
		if (enemy->_category >= 2)
				((EnemyShip*)enemy)->_weapon->_tShoot = MyConfig::getInstance().getValueAsInt("Enemy/easy_timeshot");

		enemy->_damage = MyConfig::getInstance().getValueAsInt("Enemy/easy_damage");
		
		 break;
		}
	case NORMAL:
		{
			enemy->_ai = new NormalAIBehaviour(player);
			enemy->_ai->_health_max = MyConfig::getInstance().getValueAsInt("Enemy/normal_health");
			enemy->_ai->_velocity =  MyConfig::getInstance().getValueAsInt("Enemy/normal_velocity");
			if (enemy->_category >= 2)
				((EnemyShip*)enemy)->_weapon->_tShoot = MyConfig::getInstance().getValueAsInt("Enemy/normal_timeshot");
			enemy->_damage = MyConfig::getInstance().getValueAsInt("Enemy/normal_damage");
			
		break;
		}
	case HARD :
		{
			enemy->_ai = new HardAIBehaviour(player);
			enemy->_ai->_health_max = MyConfig::getInstance().getValueAsInt("Enemy/hard_health");
			enemy->_ai->_velocity =  MyConfig::getInstance().getValueAsInt("Enemy/hard_velocity");
			if (enemy->_category >= 2)
				((EnemyShip*)enemy)->_weapon->_tShoot = MyConfig::getInstance().getValueAsInt("Enemy/hard_timeshot");
			enemy->_damage = MyConfig::getInstance().getValueAsInt("Enemy/hard_damage");
			break;
		}

	case EXTREAM :
		{
		enemy->_ai = new ExtreamAIBehaviour(player);
		enemy->_ai->_health_max = MyConfig::getInstance().getValueAsInt("Enemy/extrem_health");
		enemy->_ai->_velocity =  MyConfig::getInstance().getValueAsInt("Enemy/extrem_velocity");
		if (enemy->_category >= 2)
				((EnemyShip*)enemy)->_weapon->_tShoot = MyConfig::getInstance().getValueAsInt("Enemy/extrem_timeshot");
		enemy->_damage = MyConfig::getInstance().getValueAsInt("Enemy/extrem_damage");
		break;
		}

	}
	enemy->_ai->_parent = enemy;
	enemy->_health += enemy->_ai->_health_max;
	return enemy;
 } else if (gameObjectXML->_type == ITEM) {
	 
	 Item* item = new Item(_sceneManager, MyConfig::getInstance().getValueAsString("Mesh/Item"+category_s));
	 item->_velocity = gameObjectXML->_velocity;
	  item->_time_use =  MyConfig::getInstance().getValueAsInt("Items/Item_time");
	 item->getSceneNode()->setPosition(x,y,z);
	 item->_type = gameObjectXML->_type;
	 item->_category = gameObjectXML->_category;
	return item;
 }
}
	
