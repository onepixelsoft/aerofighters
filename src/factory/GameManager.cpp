#include "factory/GameManager.h"
#include "tools/extglobal.h"
#include "engine/GameInput.h"
#include "engine/GameListener.h"
#include "tools/MyConfig.h"
#include "game/Player.h"
#include "game/Enemy.h"
#include "game/GameState.h"
#include "game/MainState.h"

#include <SDL.h>
#include <SDL/SDL_mixer.h>

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "tools/Importer.h"
#include "game/GameXML.h"
#include "factory/GameFactory.h"

GameManager *
  GameManager::pinstance = 0;

GameManager *
GameManager::getSingleton ()
{
  if (!pinstance)
    pinstance = new GameManager ();
  return pinstance;
}

GameManager::GameManager ()
{
  _sceneManager =
    Ogre::Root::getSingletonPtr ()->createSceneManager (Ogre::ST_GENERIC);
  _camera = _sceneManager->createCamera ("MainCamera");
  _camera->setPosition (Ogre::Vector3 (0, 0, 300));
  _camera->lookAt (Ogre::Vector3 (0, 0, 0));
  _camera->setNearClipDistance (0.5f);
  _camera->setFarClipDistance (1000);

}

GameManager::~GameManager ()
{
}


void
GameManager::initialize (Ogre::RenderWindow * window)
{
  
  
  Ogre::Viewport * viewport = window->addViewport (_camera);
  viewport->setBackgroundColour (Ogre::ColourValue (0.0, 0.0, 0.0));
  double width = viewport->getActualWidth ();
  double height = viewport->getActualHeight ();
  _camera->setAspectRatio (width / height);
  loadCEGUI ();
  loadResources ();

  loadVarGlobal();

  if (!initSDL ())
	EXIT = false;

  _gameFactory = GameFactory::getSingletonPtr();

  _saveGame = new SaveGame();



  _gameState = new GameState ();
  GameXML* gameXML = new GameXML();
  
  Importer* xml = Importer::getSingletonPtr();

  #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	#ifdef _DEBUG
		xml->parseGame ("C:\\Curso\\OgreSDK_vc10_v1-8-1\\bin\\debug\\media\\game_levels.xml", gameXML);
	#else
		xml->parseGame  ("C:\\Curso\\OgreSDK_vc10_v1-8-1\\bin\\release\\media\\game_levels.xml", gameXML);
	#endif

	#else
		xml->parseGame  ("media/game_levels.xml", gameXML);
	#endif
  
	_gameState->setGameXML(gameXML);
  _mainState = new MainState ();
  _mainState->init();

  //_sceneManager->setSkyBox(true, "Examples/SpaceSkyBox", 5, 8);
   _sceneManager->setSkyBox(true, MyConfig::getInstance().getValueAsString("System/skybox"), 5, 8);

  _pHighScores = new HighScores ();
  
  _gameInput = new GameInput (window, _sceneManager, this);
  _framelistener = new GameListener (window, _sceneManager);
  _framelistener->setGameInput (_gameInput);
  _gameState->_scene = _framelistener->getScene();
  _gameState->_sceneLayer = _framelistener->getLayerScene();
  
  Ogre::Root::getSingletonPtr ()->addFrameListener (_framelistener);

  windowResized (window);

  Ogre::WindowEventUtilities::addWindowEventListener (window, this);



}

bool
GameManager::initSDL ()
{
  _pTrackManager = new TrackManager;
  _pSoundFXManager = new SoundFXManager;

  if (SDL_Init (SDL_INIT_AUDIO) < 0)
    return false;
  // Llamar a  SDL_Quit al terminar.
  atexit (SDL_Quit);

  // Inicializando SDL mixer...
  if (Mix_OpenAudio
      (MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS,
       4096) < 0)
    return false;

  // Llamar a Mix_CloseAudio al terminar.
  atexit (Mix_CloseAudio);

  return true;

}

void GameManager::loadSounds(){
	mSoundsFXmap.insert(make_pair("BigExplosion", _pSoundFXManager->load ("explosion_large_distant.ogg") ) );
	mSoundsFXmap.insert(make_pair("SmallExplosion", _pSoundFXManager->load ("explosion_large_distant.ogg") ) );
	mSoundsFXmap.insert(make_pair("PlayerExplosion", _pSoundFXManager->load ("NFF-flapping.wav") ) );
	mSoundsFXmap.insert(make_pair("HugeExplosion", _pSoundFXManager->load ("NFF-flapping.wav") ) );	
	mSoundsFXmap.insert(make_pair("PlayerShot", _pSoundFXManager->load ("NFF-laser.wav") ) );
	mSoundsFXmap.insert(make_pair("EnemyShot", _pSoundFXManager->load ("science_fiction_laser_gun_or_beam_fire_version_4.ogg") ) );		
	mSoundsFXmap.insert(make_pair("PickItem", _pSoundFXManager->load ("NFF-pick.wav") ) );	
	

}

void GameManager::addTimer (Ogre::String timer)
{
		//Ogre::OverlayManager* pOverlayMgr = Ogre::OverlayManager::getSingletonPtr();
		//Ogre::OverlayElement* elem = pOverlayMgr->getOverlayElement("timerText");
		//elem->setCaption("Timer: " + timer);
	//DEBUG>
}



void GameManager::saveGame(int level, int pts)
{
	
  _saveGame->add(level,pts);
  
}

void GameManager::unloadMainTrack()
{
	_mainTrack->unload ();
  
}


void GameManager::playMainTrack(Ogre::String sound)
{
	_mainTrack = _pTrackManager->load (sound);
		_mainTrack->play ();
  
}

void
GameManager::playFX(Ogre::String fxName)
{

	std::map<Ogre::String, SoundFXPtr>::iterator it;
	it = 	mSoundsFXmap.find(fxName);
	if(it == mSoundsFXmap.end()) {
	} 
	else {
	  SoundFXPtr sound = it->second;
		sound->play(); 
	}

}



/*
Supuestamente de aqui hacia abajo no hay que tocar
*/
void
GameManager::loadCEGUI ()
{
  CEGUI::OgreRenderer * renderer = &CEGUI::OgreRenderer::bootstrapSystem ();
  CEGUI::Scheme::setDefaultResourceGroup ("Schemes");
  CEGUI::Imageset::setDefaultResourceGroup ("Imagesets");
  CEGUI::Font::setDefaultResourceGroup ("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup ("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup ("LookNFeel");
}

void
GameManager::loadResources ()
{
  Ogre::ConfigFile cf;
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#ifdef _DEBUG
  cf.load ("C:\\Curso\\OgreSDK_vc10_v1-8-1\\bin\\debug\\resources.cfg");
#else
  cf.load ("C:\\Curso\\OgreSDK_vc10_v1-8-1\\bin\\release\\resources.cfg");
#endif

#else
  cf.load ("resources.cfg");
#endif




  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator ();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements ())
    {
      sectionstr = sI.peekNextKey ();
      Ogre::ConfigFile::SettingsMultiMap * settings = sI.getNext ();
      Ogre::ConfigFile::SettingsMultiMap::iterator i;
      for (i = settings->begin (); i != settings->end (); ++i)
	{
	  typestr = i->first;
	  datastr = i->second;
	  Ogre::ResourceGroupManager::getSingleton ().addResourceLocation
	    (datastr, typestr, sectionstr);
	}
    }
  Ogre::ResourceGroupManager::getSingleton ().initialiseAllResourceGroups ();

  
}


void GameManager::windowResized (Ogre::RenderWindow * rw)
{
  
  unsigned int width, height, depth;
  int left, top;
  rw->getMetrics (width, height, depth, left, top);

  _width = (float)width;
  _height = (float)height;
  const OIS::MouseState & ms = _gameInput->getOisMouse ()->getMouseState ();
  ms.width = width;
  ms.height = height;
  CEGUI::Size size;
  size.d_width = static_cast < float >(width);
  size.d_height = static_cast < float >(height);
  CEGUI::System::getSingleton ().notifyDisplaySizeChanged (size);
}


void GameManager::windowClosed (Ogre::RenderWindow * rw)
{
  EXIT = false;

}

void GameManager::loadVarGlobal()
{
	X_MAX = MyConfig::getInstance().getValueAsInt("System/SCREEN_WIDTH");
	X_MIN = -X_MAX;
	Y_MAX = MyConfig::getInstance().getValueAsInt("System/SCREEN_HEIGHT");
	Y_MIN = -Y_MAX;

}

void GameManager::removeAll()
{
	_framelistener->removeAll();
}
