#include <stdio.h>
#include "factory/HighScores.h"

 
using namespace std;

 
 
const char *FILENAME = "highscores";

 
template <> HighScores * Ogre::Singleton < HighScores >::msSingleton = 0;

 
HighScores & 
HighScores::getSingleton ()
{
  
assert (msSingleton);
  
return (*msSingleton);

}


 
HighScores * 
HighScores::getSingletonPtr ()
{
  
assert (msSingleton);
  
return msSingleton;

}


 
HighScores::HighScores ()
{
  
_scores.clear ();
  
_nScores = 20;

}


HighScores::~HighScores ()
{
}


 
//void HighScores::add(const string& score){
  void
HighScores::add (const char *name, int score)
{
  
char buffer[100];
  
 
sprintf (buffer, "%d %s\n", score, name);
  
string str (buffer);
  
 
read ();
  
 
if (_scores.size () < _nScores)
    {
      
_scores.push_back (str);
    
}
  
  else
    {
      
 
if (HighScores::compareScores (str, _scores.back ()))
	{
	  
_scores.pop_back ();
	  
_scores.push_back (str);
	
}
    
}
  
 
    //se da por supuesto que la lista se ordena tras insertar, al leerla debe de estar siempre ordenada
    _scores.sort (compareScores);
  
 
write ();

}


 
//void HighScores::write (const char * name, int score)
  void
HighScores::write () 
{
  
 
ofstream os (FILENAME);
  
 
if (!os.is_open ())
    
    {
      
return;
    
}
  
  else
    {
      
list < string >::iterator it;
      
 
for (it = _scores.begin (); it != _scores.end (); ++it)
	{
	  
os << *it;
	
}
    
 
 
}
  
 
os.close ();

}


 
void
HighScores::read ()
{
  
ifstream is (FILENAME);
  
string str = "";
  
 
_scores.clear ();
  
 
if (!is.is_open ())
    
    {
      
return;
    
}
  
  else
    {
      
 
while (getline (is, str))
	{
	  
str.append ("\n");
	  
_scores.push_back (str);
	
}
      
 
is.close ();
    
}

 
}


 
bool HighScores::compareScores (const string & a, const string & b)
{
  
int
    i;
  
int
    j;
  
 
sscanf (a.c_str (), "%d", &i);
  
sscanf (b.c_str (), "%d", &j);
  
 
 
return (i > j);

}


