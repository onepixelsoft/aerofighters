#include <stdio.h>
#include "factory/SaveGame.h"
#include <vector>

using namespace std;



const char *FILENAME_SG = "savegame";


template <> SaveGame * Ogre::Singleton < SaveGame >::msSingleton = 0;


SaveGame & 
	SaveGame::getSingleton ()
    
{

	assert (msSingleton);

	return (*msSingleton);

}



SaveGame * 
	SaveGame::getSingletonPtr () 
{

	assert (msSingleton);

	return msSingleton;

}



SaveGame::SaveGame () 
{

	_level = -1;
	_pts = -1;

}


SaveGame::~SaveGame ()
{
}



void
	SaveGame::add (int level, int pts)
{

	_level = level;
	_pts = pts;
	boost::asio::io_service io;
	boost::asio::deadline_timer t(io, 
			boost::posix_time::seconds(0));
	t.async_wait(boost::bind(&SaveGame::write, this));
	io.run();
	
	
	

}




void SaveGame::write () 
{


	ofstream os (FILENAME_SG);


	if (!os.is_open ())

	{

		return;

	}

	else
	{

		os << "level " << _level << " Pts " << _pts;

	}


	os.close ();

}



void SaveGame::read ()
{

	ifstream is (FILENAME_SG);

	Ogre::String str = "";

	if (!is.is_open ())
	{

		return;

	} else
	{


		while (getline (is, str))
		{

			istringstream buffer(str);
			//Ogre::String aux1 = Ogre::StringUtil::replaceAll(str, "level ", "");
			//Ogre::String aux2 = Ogre::StringUtil::replaceAll(aux1, " Pts ", "");

			Ogre::StringVector v = Ogre::StringUtil::split(str, "/level / Pts ");
			//Ogre::Vector2 v = Ogre::StringConverter::parseVector2(str);
			_level = Ogre::StringConverter::parseInt(v[0]);
			_pts = Ogre::StringConverter::parseInt(v[1]);

		}


		is.close ();

	}


}



