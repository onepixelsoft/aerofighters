#include "game/AIBehaviour.h"
#include "tools/Timer.h"
#include "engine/GameObject.h"
#include "game/Shot.h"
#include "game/Player.h"
#include "tools/extglobal.h"



void
EasyAIBehaviour::hit
(GameObject* gameObject, Timer* timer)
{

	_parent->hit(gameObject, 0);
}

void
NormalAIBehaviour::hit
(GameObject* gameObject, Timer* timer)
{

	_parent->hit(gameObject, 0);
  
  

}


void
HardAIBehaviour::hit
(GameObject* gameObject, Timer* timer)
{
  _parent->hit(gameObject, 0);
}

void
ExtreamAIBehaviour::hit
(GameObject* gameObject, Timer* timer)
{
  _parent->hit(gameObject, 0);
}

void
EasyAIBehaviour::update
(const Ogre::FrameEvent & evt, Timer* timer)
{
   
  
	float n = _parent->_velocity  * evt.timeSinceLastFrame;
 	  
	_parent->move(n,0);
	_parent->shoot(timer->getCurrentTimer());
}

void
NormalAIBehaviour::update
(const Ogre::FrameEvent & evt, Timer* timer)
{
	
	float n = (_parent->_velocity + _velocity)  * evt.timeSinceLastFrame;
 	  
	_parent->move(n,0);
	_parent->shoot(timer->getCurrentTimer());

  
	
}


void
HardAIBehaviour::update
(const Ogre::FrameEvent & evt, Timer* timer)
{
  float n = (_parent->_velocity + _velocity)  * evt.timeSinceLastFrame;
 	  
	_parent->move(n,0);
	_parent->shoot(timer->getCurrentTimer());
}

void
ExtreamAIBehaviour::update
(const Ogre::FrameEvent & evt, Timer* timer)
{
 float n = (_parent->_velocity + _velocity)  * evt.timeSinceLastFrame;
 	  
	_parent->move(n,0);
	_parent->shoot(timer->getCurrentTimer());
}


