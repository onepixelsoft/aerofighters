#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#include <unistd.h>
#endif

#include "game/Asteroid.h"
#include "factory/GameManager.h"
#include <OgreQuaternion.h>
#include "game/GameState.h"
#include "game/AIBehaviour.h"
#include "factory/GameFactory.h"
#include "tools/extglobal.h"
#include "game/Explosion.h"
#include "game/Shot.h"
#include "tools/MyConfig.h"

Asteroid::Asteroid (Ogre::SceneManager * sceneManager, const Ogre::String & name) : Enemy (sceneManager,
	    name)
{
	
	_score = MyConfig::getInstance().getValueAsInt("Enemy/asteroid_score");
	_score = _score * GameFactory::getSingletonPtr()->_config;
  int randomDir = Ogre::Math::RangeRandom(0,7);
	//eje aleatorio en el que rotara el asteroide
	switch(randomDir)
	{
		case 1:
			mRotVec = Ogre::Vector3::UNIT_X;
			break;
		case 2:	
			mRotVec = Ogre::Vector3::UNIT_Y;
			break;
		case 3:
			mRotVec = Ogre::Vector3::UNIT_Z;
			break;
		case 4:
			mRotVec = Ogre::Vector3::NEGATIVE_UNIT_X;
			break;
		case 5:
			mRotVec = Ogre::Vector3::NEGATIVE_UNIT_Y;
			break;
		case 6:
			mRotVec = Ogre::Vector3::NEGATIVE_UNIT_Z;
			break;
		default:
			mRotVec = Ogre::Vector3::UNIT_X;
	}
		 	
 
}

Asteroid::~Asteroid ()
{

}

Asteroid::Asteroid (const Asteroid & obj) :Enemy(obj)
{
  
  
  
}


void Asteroid::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState (); 
	_ai->update(evt,timer);
	//rotar los asteroides	
	_sceneNode->rotate( mRotVec, Ogre::Degree(15*evt.timeSinceLastFrame) ); 
  
}

void Asteroid::onIntersect(GameObject* gameObject, Timer* timer)
{
	_ai->hit(gameObject, timer);
}

void Asteroid::destroy()
{
	GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  GameFactory* gameFactory = gameManager->getGameFactory();
  int rand = Ogre::Math::RangeRandom(1,3); 

  if (rand) {
	  int cant = Ogre::Math::RangeRandom(2,3);
	  for(int i=0;i<cant;i++) {
		  GameObject* gameObject = gameFactory->createObjectGame(PIECE);
		  gameObject->getSceneNode()->setPosition(_sceneNode->getPosition());
		  gameState->addWorld(gameObject);
	  }
  }

  
  _remove = true;
  gameState->removePtsGo();
}

void Asteroid::hit(GameObject* gameObject, int damage)
{
	if (gameObject->_type == SHOT)
	{
		Shot* shot = static_cast<Shot*>(gameObject);
		_health -= shot->_damage + damage;
		if (_health <= 0) 
		  {
			destroy();
			Ogre::SceneManager* sceneMgr =	GameManager::getSingleton()->getSceneManager();
			Ogre::Vector3 position = _sceneNode->getPosition();	
			new Explosion( sceneMgr, position, 2.0f, "FireExplosion", "BigExplosion");
			GameManager::getSingleton()->getGameState()->scoreAdd(_score);
		  }

	} else if (gameObject->_type == PLAYER)
	{
		
		_remove = true;
			GameManager *gameManager = GameManager::getSingleton ();
			GameState *gameState = gameManager->getGameState ();
			gameState->removePtsGo();
		Ogre::SceneManager* sceneMgr =	GameManager::getSingleton()->getSceneManager();
		new Explosion( sceneMgr, _sceneNode->getPosition(), 2.0f, "FireExplosion", "BigExplosion"); 

	}
}

void Asteroid::move(float x, float y)
{
	Ogre::Vector3 position = _sceneNode->getPosition();
	_sceneNode->translate(-x,0,0);

	if (position.x < X_MIN || position.y > Y_MAX || position.y < Y_MIN)	{
		_remove = true;
		GameManager *gameManager = GameManager::getSingleton ();
		GameState *gameState = gameManager->getGameState ();
		gameState->removePtsGo();
	}
}
