#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#include <unistd.h>
#endif

#include "game/Enemy.h"
#include "factory/GameManager.h"
#include <OgreQuaternion.h>
#include "game/GameState.h"
#include "game/AIBehaviour.h"


Enemy::Enemy (Ogre::SceneManager * sceneManager, const Ogre::String & name) : GameObject (sceneManager,
	    name)
{
 
  _state = 0;
  _up = false;
  _down = false;
  _rigth = false;
  _left = false;
	//eliminar escalado  
	_sceneNode->setScale(5,5,5);
  _type = 2;
  
  
}

Enemy::~Enemy ()
{

}

Enemy::Enemy (const Enemy & obj)
{
  
  _state = 0;
  
}

void Enemy::onclick ()
{

  

}

void Enemy::mouseIn ()
{
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();

 


}

void Enemy::mouseOut ()
{
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  
}

void Enemy::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();

  
}

void Enemy::moveUp ()
{
	
	_up = true;

}

void Enemy::moveDown ()
{
	
	_down = true;

}

void Enemy::moveLeft ()
{
	
	_left = true;

}

void Enemy::moveRigth ()
{
	
	_rigth = true;

}

void Enemy::onIntersect(GameObject* gameObject, Timer* timer)
{
	
}

void Enemy::destroy()
{
	

}


void Enemy::hit(GameObject* gameObject, int damage)
{
	

}

void Enemy::move(float x, float y)
{
	

}

void Enemy::shoot(unsigned long timer)
{
	

}
