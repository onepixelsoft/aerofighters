#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#include <unistd.h>
#endif

#include "game/EnemyShip.h"
#include "factory/GameManager.h"
#include <OgreQuaternion.h>
#include "game/GameState.h"
#include "game/AIBehaviour.h"
#include "factory/GameFactory.h"
#include "tools/extglobal.h"
#include "game/Weapon.h"
#include "game/Explosion.h"
#include "tools/MyConfig.h"

EnemyShip::EnemyShip (Ogre::SceneManager * sceneManager, const Ogre::String & name) : Enemy (sceneManager,
	    name)
{

	if(_category == BOSS)
	{
		_score = MyConfig::getInstance().getValueAsInt("Enemy/boss_score");
	}else
	{	
		_score = MyConfig::getInstance().getValueAsInt("Enemy/enemyfighter_score");
	}
	
	_weapon = new Weapon();
	_weapon->_inverter = -1;
	_weapon->_shotType = ENEMY;
	_weapon->_parent = this;
	int rand = Ogre::Math::RangeRandom(1,10);
	_direction = 1;
	if (rand > 5)
		_direction = -_direction;

	//<JRC
	//creamos y cargamos el sistema de particulas para el propulsor
		Ogre::String instancePartStr =  "partricle_trail" + _sceneNode->getName();
		Ogre::String instanceNodeStr =  "partricle_Node" + _sceneNode->getName();	
		m_pTrailPart = 0;				
		m_pTrailPart = sceneManager->createParticleSystem(instancePartStr, "EnemyTrail");
		Ogre::SceneNode* pTrailNode = _sceneNode->createChildSceneNode(instanceNodeStr);
		pTrailNode->attachObject(m_pTrailPart);
		pTrailNode->translate(2, 0, 0); //colocarla en la parte de atras de la nave
  
}

EnemyShip::~EnemyShip ()
{

}

EnemyShip::EnemyShip (const EnemyShip & obj) :Enemy(obj)
{
  
  
  
}


void EnemyShip::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  _ai->update(evt,timer);
  
}

void EnemyShip::onIntersect(GameObject* gameObject, Timer* timer)
{
	_ai->hit(gameObject, timer);
}

void EnemyShip::destroy()
{
	GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  GameFactory* gameFactory = gameManager->getGameFactory();
  

  
  _remove = true;
  gameState->removePtsGo();
}


void EnemyShip::hit(GameObject* gameObject, int damage)
{
	if (gameObject->_type == SHOT && gameObject->_parent->_type != ENEMY)
	{
		Shot* shot = static_cast<Shot*>(gameObject);
		_health -= shot->_damage + damage;
		if (_health <= 0) 
		  {
			GameManager::getSingleton()->getGameState()->scoreAdd(_score);			
			_ai->remove();
			Ogre::SceneManager* sceneMgr =	GameManager::getSingleton()->getSceneManager();
			Ogre::Vector3 position = _sceneNode->getPosition();	
			if(_category == BOSS)
			{
					new Explosion( sceneMgr, position, 2.0f, "ExplosionEnemy", "HugeExplosion");
			}else
			{	
			new Explosion( sceneMgr, position, 2.0f, "FireExplosion", "BigExplosion");
			}	
		}

	} else if (gameObject->_type == PLAYER)
	{
		
		Ogre::SceneManager* sceneMgr =	GameManager::getSingleton()->getSceneManager();
		new Explosion( sceneMgr, _sceneNode->getPosition(), 2.0f, "FireExplosion", "BigExplosion"); 
		_ai->remove();
	}
}

void EnemyShip::move(float x, float y)
{
	
	y = x;
	Ogre::Vector3 position = _sceneNode->getPosition();

	if (_category == BOSS && position.x < (X_MAX - 50))
		x = 0;
	if (position.y >= Y_MAX || position.y <= Y_MIN) {
		_direction = -_direction;
	}

	if (position.x < X_MIN)	{
		_remove = true;
		GameManager *gameManager = GameManager::getSingleton ();
		GameState *gameState = gameManager->getGameState ();
		gameState->removePtsGo();
	} else {

		_sceneNode->translate(-x,y*_direction,0);
	}
	
	
}

void EnemyShip::shoot(unsigned long timer)
{
	_weapon->shoot(timer);
}
