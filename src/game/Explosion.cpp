#include "game/Explosion.h"
#include "factory/GameManager.h"
#include "game/GameState.h"

int Explosion::mID = 0;

Explosion::Explosion(){
	m_pParticle = 0;
	_sceneNode = 0;
	mStartTime = 0;
	mLifeTime = 0;
	mID++;
}

//le pasamos el sceneManager, la posicion, el tiempo de vida de la explosion y el nombre del
//sistema de particulas de la explosion que queremos crear
//WIP: ahora mismo siempre se reproduce el mismo sonido para explosiones, añadir un nuevo parametro para pasarle
//que sonido queremos reproducir para cada explosion
Explosion::Explosion(Ogre::SceneManager* pSceneMgr, Ogre::Vector3 position,  float lifeTime, const Ogre::String& particleTemplateName, const Ogre::String& soundName ): GameObject(pSceneMgr, "notent"){ 



	GameManager* pGameMgr = GameManager::getSingleton();
  GameState* pGameState = pGameMgr->getGameState();
	pGameState->addWorld(this); 

	m_pSceneMgr = pSceneMgr;

	//reproducir sonido de explosion	
	if(! (soundName == "nosound") ){
		pGameMgr->playFX(soundName);		
	}
	Ogre::String instanceStr = Ogre::StringConverter::toString(mID);

	mStartTime = Timer::getSingletonPtr()->getCurrentTimer();
	mLifeTime = lifeTime * 1000; 
	m_pParticle = pSceneMgr->createParticleSystem("Particle_Explosion_" + instanceStr , particleTemplateName); 
	
	_sceneNode->attachObject(m_pParticle);
	_sceneNode->setPosition(position);

	mID++; 


}

Explosion::~Explosion(){
	
}
Explosion::Explosion(const Explosion &){}


void Explosion::destroy(){
	
	
	//primero destruyo el sistema de particulas ligado al nodo
	m_pParticle->setVisible(false);
	m_pSceneMgr->destroyParticleSystem(m_pParticle);
	//marcandolo como _remove en el siguiente frame se llamara a destruir el gameObect
	//y sceneNode asociado
	_remove = true;
	
}

void Explosion::update(const Ogre::FrameEvent & evt, Timer* timer)
{
	//si la explosion ha sobrepasado su tiempo de vida la destruimos
	float timenow = timer->getCurrentTimer() ;
	if( (mStartTime + mLifeTime) < timenow ){
		destroy();
	}
}
