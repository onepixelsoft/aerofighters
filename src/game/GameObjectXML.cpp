#include "game/GameObjectXML.h"


GameObjectXML::GameObjectXML ()
{
	_type = 2;
	_x = 0;
	_y = 0;
	_z = 0;
	_category = 1;
	_move_ai = 1;
	_shot_ai = 0;
	_velocity = 10;
	_weapon = 0;
	_number = 1;
	_remove = 1;
	_explosion = 0;
  _build = false;
  _wait = 0;
  
}

GameObjectXML::~GameObjectXML ()
{

}
