#include <time.h>
#include "game/GameState.h"
#include "factory/GameManager.h"
#include "factory/GameFactory.h"
#include <OgreQuaternion.h>
#include "tools/MyConfig.h"
#include "game/Player.h"
#include "game/Enemy.h"
#include "game/GameXML.h"
#include "game/Pts.h"
#include "game/Level.h"
#include "game/GameObjectXML.h"
#include "tools/Timer.h"
#include "game/Weapon.h"
#include "tools/extglobal.h"
#include "game/MainState.h"


GameState::GameState () 
{
  _state = INIT;
  _timer = 0;
  _score = 0;
  _nLevel = 0;
  _nPts = 0;
  _level = 0;
  _pts = 0;
  _first = false;

}

GameState::~GameState ()
{
	delete _level;
	delete _pts;
}

//<JRC
void GameState::createHUD(){

	Ogre::OverlayManager* pOverlayMgr = Ogre::OverlayManager::getSingletonPtr();
	Ogre::Overlay *overlay = pOverlayMgr->getByName("HUD");
	

	
	//para hacer el calculo de cuanta barra de vida mostrar de acorde a la vida del jugador
	Ogre::OverlayElement* elem = pOverlayMgr->getOverlayElement("lifeBarEmpty");

	overlay->show();	
	//MUY IMPORTANTE: me falta destruir el overlay una vez que salimos del juego al menu
}

//
void GameState::updateHUD(){

	
	Ogre::OverlayManager* pOverlayMgr = Ogre::OverlayManager::getSingletonPtr();
	Ogre::OverlayElement* elem = pOverlayMgr->getOverlayElement("ScoreText");
	elem->setCaption("SCORE: "+ Ogre::StringConverter::toString(_score) );
	
	

	//actualizar barra de vida
	elem = pOverlayMgr->getOverlayElement("lifeBarEmpty");
	int barSize = elem->getWidth();		
		
	float health = static_cast<Player*>(_players[0])->_health;
	float maxHealth = static_cast<Player*>(_players[0])->_maxHealth;

	float hpRatio = health/maxHealth;
	float barWidth = barSize * hpRatio;

	Ogre::OverlayElement* healthBar = pOverlayMgr->getOverlayElement("lifeBarFull");
	
	if(health > 0)
	{
		healthBar->setWidth((int)barWidth); 
	}else
	{
		healthBar->setWidth(0); 
	}
}



//>
void GameState::scoreDecrease ()
{
	if(_score <= 0){
		_score = 0;
	}
	else{
	  _score--;
	}  
	//GameManager::getSingleton ()->addPoint (_score);
}

void GameState::scoreAdd (int i)
{
  _score += i;
  //GameManager::getSingleton ()->addPoint (_score);
}

void GameState::init ()
{
  GameManager* pGameMgr = GameManager::getSingleton ();
  Ogre::SceneManager * _sceneManager = pGameMgr->getSceneManager();
  GameFactory* gameFactory = pGameMgr->getGameFactory();
	Timer::getSingletonPtr()->reset();
	_timer = 0;
	_score = 0;
	//cargar los efectos de sonido
	pGameMgr->loadSounds();	


  _sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
  _sceneManager->setShadowTextureCount(1);
  _sceneManager->setShadowTextureSize(512);


		
	if (!_first) {
	  Ogre::Light* mainLight = _sceneManager->createLight("MainLight");
		mainLight->setType(Ogre::Light::LT_DIRECTIONAL);
		mainLight->setDirection(Ogre::Vector3(0, 0, -1));
	  mainLight->setCastShadows(true);		
		
		//posicion hardcoded sabiendo la posicion de la camara, deberia acceder mejor a la camara y poner la posicion relativa a esta
		Ogre::SceneNode* pDustNode = _sceneLayer->createChildSceneNode(Ogre::Vector3(30,0, 280)); 
		Ogre::ParticleSystem* pDustParticle;
		pDustParticle =  _sceneManager->createParticleSystem("Particle_Dust", "SpaceDustTemplate");
		pDustNode->attachObject(pDustParticle);
	//	pDustNode->setPosition(position);
	
	//sistema de particulas para particulas mas peque�as tipo estrella/polvoe estelar
		Ogre::SceneNode* pDustNode2 = _sceneLayer->createChildSceneNode(Ogre::Vector3(30,0, 280)); 
		Ogre::ParticleSystem* pDustParticle2;
		pDustParticle2 =  _sceneManager->createParticleSystem("Particle_DustSmall", "SpaceDustSmallParticles");
		pDustNode2->attachObject(pDustParticle2);
		 if (_players.size() <= 0) {
			_players.push_back(gameFactory->createObjectGame( "player", MyConfig::getInstance().getValueAsString("Mesh/Player")));
			addWorld(_players[0]);
		}
		createHUD();
	}

	restartPlayer();
	_level = _gameXML->_levels[_nLevel];
	_pts = _level->_pts[_nPts];
	_lengthGos = _pts->_lengthGos;
	loadLevel(0);
	_first = true;
	
	win = true;
}

void GameState::reset ()
{
  

}

void GameState::begin_update (const Ogre::FrameEvent & evt, Timer* timer)
{
	if (_state == INGAME)
	{
		GameManager::getSingleton()->addTimer(timer->getTimerStr());
		
	}

}

void GameState::nextPts()
{
//a ver si esto arregla el final del boss
	if(_state == WAITTOEND)
		return;
	if (_pts->getNextpts() == ENDLEVEL) 
			{
				endLevel();
				
			} else {
				//uploadLevel();
				_pts->restart();
				_nPts++;
				_pts = _level->_pts[_nPts];
				_pts->_time_init = _timer;
				_lengthGos = _pts->_lengthGos;
				GameManager::getSingleton()->saveGame(_nLevel,_nPts);
				loadLevel(0);
			}
}

void GameState::backPts()
{
	if (_nPts > 0) {
				uploadLevel();
				_nPts--;
				_pts = _level->_pts[_nPts];
				_pts->_time_init = _timer;
				_lengthGos = _pts->_lengthGos;
				loadLevel(0);
			} else if (_nLevel > 0) {
				uploadLevel();
				_nLevel--;
				_nPts = _gameXML->_levels[_nLevel]->_pts.size() -1;
				_level = _gameXML->_levels[_nLevel];
				_pts = _level->_pts[_nPts];
				_pts->_time_init = _timer;
				_lengthGos = _pts->_lengthGos;
				loadLevel(0);
			}
	
}

void GameState::nextLevel()
{
	if (_level->getNextlevel() > ENDLEVEL) 
				{

					uploadLevel();
					_nPts = 0;
					_nLevel++;
					GameManager::getSingleton()->saveGame(_nLevel,_nPts);
					_level = _gameXML->_levels[_nLevel];
					_pts = _level->_pts[_nPts];
					_pts->_time_init = _timer;
					_lengthGos = _pts->_lengthGos;
					restartPlayer();
					loadLevel(0);
				} else  
				{
					endgame();
				}
}

void GameState::endLevel()
{
/*
	if (_level->getNextlevel() > ENDLEVEL) 
	{
		Timer::getSingletonPtr()->reset();
		_timer = 0;
		_state = ENDLEVEL;
		_pts->restart();
		GameManager *gameManager = GameManager::getSingleton ();
		//gameManager->getMainState()->endlevel();
		_nPts = 0;
		endgame();
		gameManager->removeAll();*/
	//} else {

		_state = ENDLEVEL;
		_nPts = 0;
		endgame();
	//}
}


void GameState::end_update (const Ogre::FrameEvent & evt, Timer* timer)
{
	//FINAL
	if (_state == END)
	{
		GameManager *gameManager = GameManager::getSingleton ();
		gameManager->removeAll();
	}

	//chapuza para hacer que espere para terminar el juego y no lo termine de manera abrupta
	if (_state == WAITTOEND)
	{

		_timer = timer->getCurrentTimer();
		
		updateHUD();
	
		if(	(EndingTime + 2000) < timer->_timer->getMilliseconds() )
		{
			//int waitTillEnding;			
			Timer::getSingletonPtr()->reset();
			_timer = 0;
			restartPlayer();
			_pts->restart();
			_nPts = 0;
			_nLevel = 0;
			_state = END;
			GameManager *gameManager = GameManager::getSingleton ();
			gameManager->getMainState()->end();
			
		
		}	
	}
	if (_state == INGAME)
	{
		_timer = timer->getCurrentTimer();
		/*Ogre::Real deltaT = evt.timeSinceLastFrame;
		int fps = 1.0 / deltaT;
		Ogre::OverlayManager* pOverlayMgr = Ogre::OverlayManager::getSingletonPtr();
		Ogre::OverlayElement* elem = pOverlayMgr->getOverlayElement("DebugText");
		elem->setCaption("FPS: "+ Ogre::StringConverter::toString(fps) );		
*/
		updateHUD();

		if (isNextPts()) {
			nextPts();
		}

		if (_pts) {
			viewWaitsLevel();
		}
	} 
}
//FINAL
void GameState::endgame()
{
	EndingTime = Timer::getSingletonPtr()->_timer->getMilliseconds();
	_state = WAITTOEND;
	
/*Timer::getSingletonPtr()->reset();
	_timer = 0;
	restartPlayer();
	_pts->restart();
	_nPts = 0;
	_nLevel = 0;
	_state = END;
	GameManager *gameManager = GameManager::getSingleton ();
	gameManager->getMainState()->end();
*/
}

void GameState::setState (short state)
{
  _state = state;
}

void GameState::setConfig (short config)
{
	_config = config;
}
/* Este funcion es para controlar la pulsaci�n continua de una tecla*/
void GameState::keyboard(OIS::Keyboard* keyboard, Timer* timer)
{
	for (std::vector<GameObject*>::iterator p = _players.begin(); p != _players.end() && _state == INGAME; p++) 
	{
		Player* player = static_cast<Player*>(*p);

		if(keyboard->isKeyDown(OIS::KC_UP) || keyboard->isKeyDown(OIS::KC_W)) {
			player->moveUp();
		} 
		if(keyboard->isKeyDown(OIS::KC_DOWN) || keyboard->isKeyDown(OIS::KC_S)) {
			player->moveDown();
		}
		if(keyboard->isKeyDown(OIS::KC_RIGHT) || keyboard->isKeyDown(OIS::KC_D)) {
			player->moveRigth();
		}
		if(keyboard->isKeyDown(OIS::KC_LEFT) || keyboard->isKeyDown(OIS::KC_A)) {
			player->moveLeft();
		}

		if(keyboard->isKeyDown(OIS::KC_SPACE)) {
			player->_weapon->shoot(timer->getCurrentTimer());
		} 

		
		
	}

		
}
/* Este funcion es para controlar la pulsaci�n unica de una tecla*/
void GameState::keyboard (const OIS::KeyEvent & evt, Timer* timer)
{

	if (_state == INGAME)
	{
		if(evt.key == OIS::KC_2) {
				nextPts();
			} else 	if(evt.key == OIS::KC_1) {
				backPts();
			} 
	}		
}

void GameState::addWorld(GameObject* gameObject)
{
	GameManager *gameManager = GameManager::getSingleton ();
	Ogre::SceneManager * _sceneManager = gameManager->getSceneManager();
	_scene->addChild (gameObject->getSceneNode());
}

void GameState::removeWorld(GameObject* gameObject)
{
	GameManager *gameManager = GameManager::getSingleton ();
	Ogre::SceneManager * _sceneManager = gameManager->getSceneManager();
	_scene->removeChild(gameObject->getSceneNode());
	
	
}

void GameState::createWorldLevel()
{


	GameManager *gameManager = GameManager::getSingleton ();
	GameFactory* gameFactory = gameManager->getGameFactory();
	
	if (_level && _pts) {
		
		
		for (std::vector<GameObjectXML*>::iterator p = _pts->_gosXml.begin(); p != _pts->_gosXml.end(); p++) 
		{
			GameObjectXML* goxml = static_cast<GameObjectXML*>(*p);

			if (_timer >= (goxml->_wait + _pts->_time_init) && !goxml->_build) {
				goxml->_build = true;
				for (int i=0;i<goxml->_number;i++) {
					GameObject* gameObject = gameFactory->createObjectGame(goxml);
					addWorld(gameObject);
					

				}
			}
			
		}
		
		
	}
	
		
	
}

void GameState::removeWorldLevel(Level* level)
{

	short _aux_state = _state;
	_state = REMOVEALL;
	GameManager *gameManager = GameManager::getSingleton ();
	gameManager->removeAll();
	_pts->restart();
	_state = _aux_state;
}


void GameState::setGameXML(GameXML* gameXML)
{
	_gameXML = gameXML;
}

void GameState::setLevel(int level)
{
	_nLevel = level;
}

void GameState::setPts(int pts)
{
	_nPts = pts;
}

void GameState::loadLevel(int time_wait)
{
	if (_gameXML->_levels[_nLevel])
	{
		boost::asio::io_service io;
		boost::asio::deadline_timer t(io, 
		boost::posix_time::seconds(time_wait));
		t.async_wait(boost::bind(&GameState::createWorldLevel, this));
		io.run();
		//createWorldLevel(_gameXML->_levels[_nLevel]);
		
	}
}

void GameState::uploadLevel()
{
	if (_level)
		removeWorldLevel(_level);
}

bool GameState::isNextPts()
{
	return !_lengthGos;
	
}

void GameState::removePtsGo()
{
	_lengthGos--;
}



void GameState::restart()
{
	Timer::getSingletonPtr()->reset();
	_timer = 0;
	_score = 0;
	uploadLevel();
	_pts = _level->_pts[_nPts];
	_pts->_time_init = _timer;
	_lengthGos = _pts->_lengthGos;
	loadLevel(0);
	restartPlayer();
	_state = INGAME;
}

void GameState::viewWaitsLevel()
{
	for (std::vector<GameObjectXML*>::iterator p = _pts->_gosXml.begin(); p != _pts->_gosXml.end(); p++) 
	{
		GameObjectXML* goxml = static_cast<GameObjectXML*>(*p);
		if (_timer >= (goxml->_wait + _pts->_time_init) && !goxml->_build) {
			loadLevel(0);
		}
			 
	}
		
	
}

void GameState::backmenu()
{
	uploadLevel();
	_nPts = 0;
	_nLevel = 0;
}

void GameState::restartPlayer()
{
	for (std::vector<GameObject*>::iterator p = _players.begin(); p != _players.end(); p++) 
	{
		Player* player = static_cast<Player*>(*p);
		player->respawn();
		
		
	}
}
