#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#include <unistd.h>
#endif

#include "game/Item.h"
#include "factory/GameManager.h"
#include "game/GameState.h"
#include "tools/extglobal.h"
#include "game/Player.h"
#include "game/Weapon.h"
#include "tools/Timer.h"
#include "game/Explosion.h"

Item::Item (Ogre::SceneManager * sceneManager, const Ogre::String & name) : GameObject (sceneManager,
	    name)
{
 
  
 _sceneNode->setScale(5,5,5);
 _time_use = 6000;
 _inUse = false;
  
}

Item::~Item ()
{
	
}

Item::Item (const Item & obj)
{
  
  
  
}

void Item::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  
  Ogre::Vector3 position = _sceneNode->getPosition();
  float n = _velocity  * evt.timeSinceLastFrame;
 	  
	_sceneNode->translate(-n,0,0);
  if ((position.x < X_MIN || position.y > Y_MAX || position.y < Y_MIN) && _category == 1)
	_remove = true;

  if (_time_init > 0 && _category == 2 && timer->getCurrentTimer() > (_time_init + 10000) && _player)
  {
	  _player->_weapon->_category = 1;
	  _player = 0;
	  _remove = true;
  }
  
}


void Item::onIntersect(GameObject* gameObject, Timer* timer)
{
	if (gameObject->_type == PLAYER && !_inUse) 
	{
		//mostrar efecto al recoger el item
		new Explosion(GameManager::getSingleton()->getSceneManager(), _sceneNode->getPosition(), 1, "ItemSpark", "PickItem");

		Player* player = static_cast<Player*>(gameObject);
		if (_category == 1)
		{
			
			player->_health = 100;
			_remove = true;
		} else if (_category == 2) {
			player->_weapon->_category = 2;
			_time_init = timer->getCurrentTimer();
			_player = player;
			_sceneNode->setVisible(false);
		}

		_inUse = true;
		

	}
}
