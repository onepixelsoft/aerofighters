#include "factory/GameManager.h"
#include "game/MainState.h"
#include "game/GameState.h"
#include "tools/extglobal.h"
#include "tools/Timer.h"
#include "factory/GameFactory.h"
#include "factory/SaveGame.h"

MainState::MainState()
{
	_sheetGame = 0;
  
}

MainState::~MainState()
{

}

void MainState::init()
{
	createSceneInit ();
	createSceneOptions ();
	createSceneCredits ();
	createScenePause ();
	createSceneHighScores ();
	createSceneEndWin ();
	createSceneEndLose ();
}

void MainState::begin_update(const Ogre::FrameEvent& evt)
{

}

void MainState::end_update(const Ogre::FrameEvent& evt)
{

}

void MainState::setState(short state)
{
	_state = state;
}

void MainState::setConfig(short config)
{
	_config = config;
}

void
MainState::createSceneInit ()
{
  CEGUI::SchemeManager::getSingleton ().create ("TaharezLook.scheme");
  CEGUI::System::getSingleton ().setDefaultFont ("DejaVuSans-10");
  CEGUI::System::getSingleton ().setDefaultMouseCursor ("TaharezLook",
							"MouseArrow");

	_sheetInit = CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow","Init");

  CEGUI::Window * layout =
    CEGUI::WindowManager::getSingleton ().loadWindowLayout ("AFMainMenu.layout");


  CEGUI::Window * exitButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("MainMenu/MainMenuBG/ExitB");
  exitButton->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::quit,
							this));

  CEGUI::Window * playButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("MainMenu/MainMenuBG/NewGameB");
  playButton->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::
							createSceneGame,
							this));

  CEGUI::Window * loadButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("MainMenu/MainMenuBG/MainMenu_LoadGameB");
  loadButton->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::
							loadGame,
							this));
  

  CEGUI::Window * configButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("MainMenu/MainMenuBG/ConfigurationB");
  configButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				CEGUI::Event::Subscriber (&MainState::
							  visibleSceneOptions,
							  this));

  CEGUI::Window * credits =
    CEGUI::WindowManager::getSingleton ().getWindow ("MainMenu/MainMenuBG/CreditsB");
  credits->subscribeEvent (CEGUI::PushButton::EventClicked,
			   CEGUI::Event::Subscriber (&MainState::
						     viewCredits, this));


  //<JRC:boton en el menu principal para acceder a los maximas puntuaciones
  CEGUI::Window * scores =
    CEGUI::WindowManager::getSingleton ().getWindow ("MainMenu/MainMenuBG/HighscoresB");
  scores->subscribeEvent (CEGUI::PushButton::EventClicked,
			  CEGUI::Event::Subscriber (&MainState::viewScores,
						    this));
  //>             

  _sheetInit->addChildWindow (layout);
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);

  _sheetInit->setVisible (true);

  GameManager::getSingleton()->playMainTrack("in_the_box_mellow_techno_trance_loop_120_bpm.wav");
 



}

void
MainState::createSceneOptions ()
{
		CEGUI::MouseCursor::getSingleton().show(); 
  _sheetConfig = CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow","conf");
    
  CEGUI::Window * layout =
    CEGUI::WindowManager::getSingleton ().
    loadWindowLayout ("AFConfigurationMenu.layout");

  CEGUI::Window * backButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("ConfigurationMenu/ConfigurationMenuBG/ConfigurationMenuOK");
  backButton->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::back,
							this));

  CEGUI::RadioButton * radioButton =
    (CEGUI::RadioButton *) CEGUI::WindowManager::getSingleton ().
    getWindow ("ConfigurationMenu/ConfigurationMenuBG/Easy");
  radioButton->setSelected (true);
	  
_sheetConfig->addChildWindow (layout);

  _sheetConfig->setVisible (false);



}

void
MainState::createSceneCredits ()
{
		CEGUI::MouseCursor::getSingleton().show(); 
	 _sheetCredits = CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow","credit");

  CEGUI::Window * layout =
    CEGUI::WindowManager::getSingleton ().
    loadWindowLayout ("AFCredits.layout");

  CEGUI::Window * backCre =
    CEGUI::WindowManager::getSingleton ().getWindow ("CreditsRoot/CreditsBG/ExitB");
  backCre->subscribeEvent (CEGUI::PushButton::EventClicked,
			   CEGUI::Event::Subscriber (&MainState::back,
						     this));
  _sheetCredits->addChildWindow (layout);
	
	CEGUI::Window * creditsText = CEGUI::WindowManager::getSingleton().getWindow("CreditsRoot/CreditsBG/CreditsWindow");
  creditsText->setText ("[colour='FFFFFFFF']AUTHORS\nManuel Jimenez\nJorge Rosique\n \nCOLLABORATORS:\nTomas Castello\nJuan Larranaga\n \nSOUND EFFECTS:\nhttp://www.freesfx.co.uk\nwww.NoiseForFun.com\n \nMUSIC\nwww.PlayOnLoop.com\n \nFX TEXTURES\nhttp://xypter.deviantart.com\n \n3D MODELS\nwww.turbosquid.com/Search/Artists/Angryfly");		
	
  _sheetCredits->setVisible (false);



}


void
MainState::createSceneHighScores ()
{
		CEGUI::MouseCursor::getSingleton().show(); 
  _sheetHighScores = CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow","scores");
  
  CEGUI::Window * layout =
    CEGUI::WindowManager::getSingleton ().
    loadWindowLayout ("AFHighscoresMenu.layout");

  CEGUI::Window * exitScoresButton =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("Highscores/HihgscoreBG/ExitB");
  exitScoresButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				    CEGUI::Event::Subscriber (&MainState::
							      back, this));
  _sheetHighScores->addChildWindow (layout);

  _sheetHighScores->setVisible (false);

}


void
MainState::createSceneEndLose ()
{
	CEGUI::MouseCursor::getSingleton().show(); 

  _sheetEndGameLose = CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow","gameOverlose");
    
  CEGUI::Window * layout = CEGUI::WindowManager::getSingleton ().
    loadWindowLayout ("AFEndLose.layout");

  CEGUI::Window * ContinueButton =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("AFEndLoseRoot/BG/ContinueB");
  ContinueButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				  CEGUI::Event::Subscriber (&MainState::
							    submitScore,
							    this));

  CEGUI::Window * MainMenuButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("AFEndLoseRoot/BG/ExitB");
  MainMenuButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				  CEGUI::Event::Subscriber (&MainState::
							    backmenu, this));

  CEGUI::Window * restartButton =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("AFEndLoseRoot/BG/RestartB");
  restartButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				 CEGUI::Event::Subscriber (&MainState::
							   restart, this));

  _sheetEndGameLose->addChildWindow (layout);
  _sheetEndGameLose->setVisible (false);
}



void
MainState::createSceneEndWin ()
{
	CEGUI::MouseCursor::getSingleton().show(); 
 

  _sheetEndGameWin = CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow","gameOverwin");
    
  CEGUI::Window * layout = CEGUI::WindowManager::getSingleton ().
    loadWindowLayout ("AFEndWin.layout");

  CEGUI::Window * ContinueButton =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("AFEndWinRoot/BG/ContinueB");
  ContinueButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				  CEGUI::Event::Subscriber (&MainState::
							    submitScore,
							    this));

  CEGUI::Window * MainMenuButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("AFEndWinRoot/BG/ExitB");
  MainMenuButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				  CEGUI::Event::Subscriber (&MainState::
							    backmenu, this));

  CEGUI::Window * restartButton =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("AFEndWinRoot/BG/RestartB");
  restartButton->subscribeEvent (CEGUI::PushButton::EventClicked,
				 CEGUI::Event::Subscriber (&MainState::
							   restart, this));

  _sheetEndGameWin->addChildWindow (layout);
  _sheetEndGameWin->setVisible (false);
}





//>

void
MainState::createScenePause ()
{
  _sheetPause =
    CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow",
							"Pause");
  CEGUI::Window * layout =
    CEGUI::WindowManager::getSingleton ().
    loadWindowLayout ("AFPauseMenu.layout");

  CEGUI::Window * resumeGame =
    CEGUI::WindowManager::getSingleton ().getWindow ("PauseMenu/PauseMenuBG/ResumeB");
  resumeGame->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::resume,
							this));

  CEGUI::Window * backtomenu =
    CEGUI::WindowManager::getSingleton ().getWindow ("PauseMenu/PauseMenuBG/BackB");
  backtomenu->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::
							backmenu, this));

  CEGUI::Window * backButton =
    CEGUI::WindowManager::getSingleton ().getWindow ("PauseMenu/PauseMenuBG/exitB");
  backButton->subscribeEvent (CEGUI::PushButton::EventClicked,
			      CEGUI::Event::Subscriber (&MainState::quit,
							this));

  _sheetPause->addChildWindow (layout);

  _sheetPause->setVisible (false);



}

bool
MainState::createSceneGame (const CEGUI::EventArgs & e)
{
	CEGUI::MouseCursor::getSingleton().hide();
  this->_sheetInit->setVisible (false);
 GameManager* gameManager = GameManager::getSingleton();
 gameManager->getGameState()->setLevel(0);
	gameManager->getGameState()->setPts(0);
  gameManager->getGameState()->init ();
  gameManager->getGameState()->setState (INGAME);
 GameManager::getSingleton()->saveGame(0,0);
 
  if (!this->_sheetGame)
    {
      this->_sheetGame =
	CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow",
							    "Game");

      CEGUI::Window * layout =
	CEGUI::WindowManager::getSingleton ().
	loadWindowLayout ("Memory_game.layout");
      this->_sheetGame->addChildWindow (layout);
    }

  CEGUI::System::getSingleton ().setGUISheet (this->_sheetGame);
  this->_sheetGame->setVisible (true);

	gameManager->unloadMainTrack();	
  gameManager->playMainTrack("POL-iron-suit-short.wav");
 	 

  return true;
}

bool
MainState::visibleSceneOptions (const CEGUI::EventArgs & e)
{
  GameManager::getSingleton()->getGameState()->setState (CONFIG);
  _sheetInit->setVisible (false);
  _sheetConfig->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetConfig);
	CEGUI::MouseCursor::getSingleton().show(); 
  return true;
}

bool
MainState::loadGame (const CEGUI::EventArgs & e)
{
	SaveGame* saveGame = GameManager::getSingleton()->getSaveGame();
	saveGame->read();
	GameManager::getSingleton()->getGameState()->setLevel(saveGame->_level);
	GameManager::getSingleton()->getGameState()->setPts(saveGame->_pts);
	CEGUI::MouseCursor::getSingleton().hide();
  this->_sheetInit->setVisible (false);
 GameManager* gameManager = GameManager::getSingleton();
  gameManager->getGameState()->init ();
  gameManager->getGameState()->setState (INGAME);
 
 
  if (!this->_sheetGame)
    {
      this->_sheetGame =
	CEGUI::WindowManager::getSingleton ().createWindow ("DefaultWindow",
							    "Game");

      CEGUI::Window * layout =
	CEGUI::WindowManager::getSingleton ().
	loadWindowLayout ("Memory_game.layout");
      this->_sheetGame->addChildWindow (layout);
    }

  CEGUI::System::getSingleton ().setGUISheet (this->_sheetGame);
  this->_sheetGame->setVisible (true);

	gameManager->unloadMainTrack();	
  gameManager->playMainTrack("POL-iron-suit-short.wav");
  
  return true;
}

void
MainState::pause ()
{
	
  if (GameManager::getSingleton()->getGameState()->getState () == INGAME)
    {
      _sheetGame->setVisible (false);
			CEGUI::MouseCursor::getSingleton().show(); 
      GameManager::getSingleton()->getGameState()->setState (INPAUSE);
      _sheetPause->setVisible (true);
      CEGUI::System::getSingleton ().setGUISheet (_sheetPause);
	  Timer::getSingleton().pause();
    }
}


void
MainState::end ()
{
	CEGUI::MouseCursor::getSingleton().show(); 

	if(GameManager::getSingleton()->getGameState()->win)
	{
		
		//mostrar la puntuacion final obtenida
		CEGUI::Window * showPoints = CEGUI::WindowManager::getSingleton ().getWindow ("AFEndWinRoot/BG/ScoreText");
		showPoints->setText ("Has conseguido una puntuacion de: " +
				     (CEGUI::PropertyHelper::
				intToString (GameManager::getSingleton()->getGameState()->getScore ())));
	
		if (GameManager::getSingleton()->getGameState()->getState () == END)
		  {

		    _sheetGame->setVisible (false);
		    _sheetEndGameWin->setVisible (true);

		    CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameWin);
				GameManager::getSingleton()->unloadMainTrack(); 
				GameManager::getSingleton()->playMainTrack("freedom.wav");
		  }
	}
	else
	{

		//mostrar la puntuacion final obtenida
		CEGUI::Window * showPoints = CEGUI::WindowManager::getSingleton ().getWindow ("AFEndLoseRoot/BG/ScoreText");
		showPoints->setText ("Has conseguido una puntuacion de: " +
				     (CEGUI::PropertyHelper::
				intToString (GameManager::getSingleton()->getGameState()->getScore ())));

		if (GameManager::getSingleton()->getGameState()->getState () == END)
		  {

		    _sheetGame->setVisible (false);
		    _sheetEndGameLose->setVisible (true);

		    CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameLose);
				GameManager::getSingleton()->unloadMainTrack(); 
				GameManager::getSingleton()->playMainTrack("POL-droid-walk-short.wav");

		  }
	}
}

void
MainState::endlevel ()
{
/*
	CEGUI::MouseCursor::getSingleton().show(); 
  //mostrar la puntuacion final obtenida
 CEGUI::Window * showPoints = CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Text");
  showPoints->setText ("Has conseguido una puntuacion de: " +
		       (CEGUI::PropertyHelper::
			intToString (GameManager::getSingleton()->getGameState()->getScore ())));

			   CEGUI::Window * continueGame =
    CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Continue");
  continueGame->setVisible(true);
			

  if (GameManager::getSingleton()->getGameState()->getState () == ENDLEVEL)
    {

      _sheetGame->setVisible (false);
      _sheetEndGame->setVisible (true);

      CEGUI::System::getSingleton ().setGUISheet (_sheetEndGame);
			GameManager::getSingleton()->unloadMainTrack(); 
			GameManager::getSingleton()->playMainTrack("POL-droid-walk-short.wav");
    }
	CEGUI::MouseCursor::getSingleton().show();*/
	CEGUI::MouseCursor::getSingleton().show(); 

		if(GameManager::getSingleton()->getGameState()->win)
		{
		
			//mostrar la puntuacion final obtenida
			CEGUI::Window * showPoints = CEGUI::WindowManager::getSingleton ().getWindow ("AFEndWinRoot/BG/ScoreText");
			showPoints->setText ("Has conseguido una puntuacion de: " +
						   (CEGUI::PropertyHelper::
					intToString (GameManager::getSingleton()->getGameState()->getScore ())));
		
			if (GameManager::getSingleton()->getGameState()->getState () == END)
				{

				  _sheetGame->setVisible (false);
				  _sheetEndGameWin->setVisible (true);

				  CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameWin);
					GameManager::getSingleton()->unloadMainTrack(); 
					GameManager::getSingleton()->playMainTrack("freedom.wav");
				}
		}
		else
		{

			//mostrar la puntuacion final obtenida
			CEGUI::Window * showPoints = CEGUI::WindowManager::getSingleton ().getWindow ("AFEndLoseRoot/BG/ScoreText");
			showPoints->setText ("Has conseguido una puntuacion de: " +
						   (CEGUI::PropertyHelper::
					intToString (GameManager::getSingleton()->getGameState()->getScore ())));
			/*CEGUI::Window * continueGame =
				CEGUI::WindowManager::getSingleton ().getWindow ("RootHS/GameOver/Continue");
			continueGame->setVisible(false);*/
			if (GameManager::getSingleton()->getGameState()->getState () == END)
				{

				  _sheetGame->setVisible (false);
				  _sheetEndGameLose->setVisible (true);

				  CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameLose);
					GameManager::getSingleton()->unloadMainTrack(); 
					GameManager::getSingleton()->playMainTrack("POL-droid-walk-short.wav");

				}
		}
	
}

bool
MainState::resume (const CEGUI::EventArgs & e)
{
  GameManager::getSingleton()->getGameState()->setState (INGAME);
  _sheetPause->setVisible (false);
  _sheetGame->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetGame);
  CEGUI::MouseCursor::getSingleton().hide();

  return true;
}

bool
MainState::restart (const CEGUI::EventArgs & e)
{
 
	CEGUI::MouseCursor::getSingleton().show();

	if(GameManager::getSingleton()->getGameState()->win){

	 _sheetEndGameWin->setVisible (false);
  CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameWin);		
	
  CEGUI::Window * nameField = CEGUI::WindowManager::getSingleton().getWindow("InsertNameW");
  //namefield
  CEGUI::String name = nameField->getText ();  
  //score es la puntuacion que pasaremos para que guarde, cuando este implementado
  //el sistema de puntuacion del juego habra que acceder a la puntuacion final y pasarla como parametro
GameManager* gameManager = GameManager::getSingleton();
		gameManager->getHighScores ()->add (name.c_str (), gameManager->getGameState()->getScore ());
	}
	else{
	 _sheetEndGameLose->setVisible (false);
	  CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameLose);
		 CEGUI::Window * nameField = CEGUI::WindowManager::getSingleton().getWindow("InsertName");
		//namefield
		CEGUI::String name = nameField->getText ();  
		//score es la puntuacion que pasaremos para que guarde, cuando este implementado
		//el sistema de puntuacion del juego habra que acceder a la puntuacion final y pasarla como parametro
		GameManager* gameManager = GameManager::getSingleton();
		gameManager->getHighScores ()->add (name.c_str (), gameManager->getGameState()->getScore ());
	}
	GameManager* gameManager = GameManager::getSingleton();
	 //volvemos a main menu una vez guardada la puntuacion
  gameManager->getGameState()->restart ();
  gameManager->getGameState()->setState (INGAME);
	this->_sheetGame->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (this->_sheetGame);
  gameManager->unloadMainTrack();
  gameManager->playMainTrack("POL-iron-suit-short.wav");
  
	GameManager::getSingleton()->getGameState()->win = true;

  

  return true;
}

bool
MainState::backmenu (const CEGUI::EventArgs & e)
{
	CEGUI::MouseCursor::getSingleton().show();
  GameManager::getSingleton()->getGameState()->setState (INIT);
  GameManager::getSingleton()->getGameState()->backmenu();
  _sheetInit->setVisible (true);
  _sheetPause->setVisible (false);
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
  
	GameManager::getSingleton()->unloadMainTrack(); 
	GameManager::getSingleton()->playMainTrack("in_the_box_mellow_techno_trance_loop_120_bpm.wav");
  return true;
}

bool
MainState::quit (const CEGUI::EventArgs & e)
{
  EXIT = false;
  return true;
}

bool
MainState::back (const CEGUI::EventArgs & e)
{
	CEGUI::MouseCursor::getSingleton().show();
  CEGUI::RadioButton * radioButton =
    (CEGUI::RadioButton *) CEGUI::WindowManager::getSingleton ().
    getWindow ("ConfigurationMenu/ConfigurationMenuBG/Easy");
  CEGUI::RadioButton * radioSelected =
    radioButton->getSelectedButtonInGroup ();
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
  if (radioSelected)
    {
	 GameManager::getSingleton()->getGameState()->
	setConfig (CEGUI::PropertyHelper::
		   stringToInt (radioSelected->getProperty ("ID")));
	  GameManager::getSingleton()->getGameFactory()->
	setConfig (CEGUI::PropertyHelper::
		   stringToInt (radioSelected->getProperty ("ID")));
	  
    }
  this->_sheetConfig->setVisible (false);
  GameManager::getSingleton()->getGameState()->setState (INIT);
  _sheetInit->setVisible (true);
  return true;
}

bool
MainState::viewCredits (const CEGUI::EventArgs & e)
{
	CEGUI::MouseCursor::getSingleton().show();
  _sheetInit->setVisible (false);
  _sheetCredits->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetCredits);
  return true;
}

//<JRC:
bool
MainState::viewScores (const CEGUI::EventArgs & e)
{
	CEGUI::MouseCursor::getSingleton().show();
  _sheetInit->setVisible (false);
  _sheetHighScores->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (_sheetHighScores);
  //acceso a la ventana que muestra el texto
  CEGUI::Window * showText =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("Highscores/HihgscoreBG/HighscoresWindow");
  showText->setText ("");	//resetear texto porque sino cada vez que se entra en viewScores se duplica lo que se muestra lista
  GameManager::getSingleton()->getHighScores ()->read ();
  //imprimir la lista de las puntuaciones
  std::list < string > scores =  GameManager::getSingleton()->getHighScores ()->getScoresList ();
  list < string >::iterator it;

  for (it = scores.begin (); it != scores.end (); ++it)
  {
	showText->appendText (*it);
  }

  return true;
}

bool
MainState::submitScore (const CEGUI::EventArgs & e)
{
	CEGUI::MouseCursor::getSingleton().show();
  _sheetEndGameWin->setVisible (false);
  _sheetEndGameLose->setVisible (false);


	if(GameManager::getSingleton()->getGameState()->win)
	{
		  CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameWin);	
		CEGUI::Window * nameField =
    CEGUI::WindowManager::getSingleton ().
    getWindow ("InsertNameW");
  //namefield
  CEGUI::String name = nameField->getText ();  
  //score es la puntuacion que pasaremos para que guarde, cuando este implementado
  //el sistema de puntuacion del juego habra que acceder a la puntuacion final y pasarla como parametro
  GameManager* gameManager = GameManager::getSingleton();
  gameManager->getHighScores ()->add (name.c_str (), gameManager->getGameState()->getScore ());

	}  
	else
	{
					  CEGUI::System::getSingleton ().setGUISheet (_sheetEndGameLose);	
			CEGUI::Window * nameField =
		  CEGUI::WindowManager::getSingleton ().
		  getWindow ("InsertName");
		//namefield
		CEGUI::String name = nameField->getText ();  
		//score es la puntuacion que pasaremos para que guarde, cuando este implementado
		//el sistema de puntuacion del juego habra que acceder a la puntuacion final y pasarla como parametro
		GameManager* gameManager = GameManager::getSingleton();
		gameManager->getHighScores ()->add (name.c_str (), gameManager->getGameState()->getScore ());
		
	}
/*
	GameManager* gameManager = GameManager::getSingleton();
  //volvemos a main menu una vez guardada la puntuacion
  gameManager->getGameState()->nextLevel ();
  gameManager->getGameState()->setState (INGAME);
	this->_sheetGame->setVisible (true);
  CEGUI::System::getSingleton ().setGUISheet (this->_sheetGame);
  gameManager->unloadMainTrack();
  gameManager->playMainTrack("POL-iron-suit-short.wav");
  GameManager::getSingleton()->getGameState()->win = true;
*/


//////////////////////////
		CEGUI::MouseCursor::getSingleton().show();
  CEGUI::RadioButton * radioButton =
    (CEGUI::RadioButton *) CEGUI::WindowManager::getSingleton ().
    getWindow ("ConfigurationMenu/ConfigurationMenuBG/Easy");
  CEGUI::RadioButton * radioSelected =
    radioButton->getSelectedButtonInGroup ();
  CEGUI::System::getSingleton ().setGUISheet (_sheetInit);
  if (radioSelected)
    {
	 GameManager::getSingleton()->getGameState()->
	setConfig (CEGUI::PropertyHelper::
		   stringToInt (radioSelected->getProperty ("ID")));
	  GameManager::getSingleton()->getGameFactory()->
	setConfig (CEGUI::PropertyHelper::
		   stringToInt (radioSelected->getProperty ("ID")));
	  
    }
  this->_sheetConfig->setVisible (false);
  GameManager::getSingleton()->getGameState()->setState (INIT);
  _sheetInit->setVisible (true);
	 GameManager* gameManager	 = GameManager::getSingleton();
  gameManager->unloadMainTrack();
  gameManager->playMainTrack("in_the_box_mellow_techno_trance_loop_120_bpm.wav");
  GameManager::getSingleton()->getGameState()->win = true;


  
  return true;

}

//>



/*
Supuestamente de aqui hacia abajo no hay que tocar
*/
void
MainState::loadCEGUI ()
{
  CEGUI::OgreRenderer * renderer = &CEGUI::OgreRenderer::bootstrapSystem ();
  CEGUI::Scheme::setDefaultResourceGroup ("Schemes");
  CEGUI::Imageset::setDefaultResourceGroup ("Imagesets");
  CEGUI::Font::setDefaultResourceGroup ("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup ("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup ("LookNFeel");
}
