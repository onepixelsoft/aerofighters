#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#include <unistd.h>
#endif

#include "game/Piece.h"
#include "factory/GameManager.h"
#include <OgreQuaternion.h>
#include "game/GameState.h"
#include "game/Explosion.h"
#include "tools/extglobal.h"
#include "tools/MyConfig.h"

Piece::Piece (Ogre::SceneManager * sceneManager, const Ogre::String & name) : GameObject (sceneManager,
	    name)
{
 	//SOLO PARA DEBUG, MUY IMPORTANTE!!!!!!!!!!!!!!!!!!!!!
 	_sceneNode->setScale(2,2,2); 
 	//SOLO PARA DEBUG, MUY IMPORTANTE!!!!!!!!!!!!!!!!!!!!!
  _type = 3;
  _damage = 0;
	_score = MyConfig::getInstance().getValueAsInt("Enemy/piece_score");

}

Piece::~Piece ()
{
	delete _direction;
}

Piece::Piece (const Piece & obj)
{
  
  
  
}

void Piece::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  //GameManager *gameManager = GameManager::getSingleton ();
  //GameState *gameState = gameManager->getGameState ();
  
  _sceneNode->translate(_direction->x* evt.timeSinceLastFrame, _direction->y* evt.timeSinceLastFrame, 0);
  if (isOutlimit()){
	  _remove = true;		  
	}
}


void Piece::onIntersect(GameObject* gameObject, Timer* timer)
{
	//<JRC

	if (gameObject->_type != ITEM)
	{
		Ogre::SceneManager* sceneMgr =	 GameManager::getSingleton()->getSceneManager();
		Ogre::Vector3 position = _sceneNode->getPosition();
		position.z += 20;	
		new Explosion( sceneMgr, position, 0.5f, "SmallFireExplosion", "BigExplosion");
		GameManager::getSingleton()->getGameState()->scoreAdd(_score);	
	//>

		_remove = true;
	}
}
