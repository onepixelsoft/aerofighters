#include "game/Player.h"
#include "factory/GameManager.h"
#include <OgreQuaternion.h>
#include "game/GameState.h"
#include "tools/extglobal.h"
#include "game/Weapon.h"
#include "game/Piece.h"
#include "game/Enemy.h"
#include "game/Shot.h"
#include "game/Explosion.h"

Player::Player (Ogre::SceneManager * sceneManager, const Ogre::String & name) : GameObject (sceneManager,
	    name)
{

  _state = 0;
  // TCR ------------ animaciones  
  _animState = _ent->getAnimationState("up");
  _animState2 = _ent->getAnimationState("down");
  _animState3 = _ent->getAnimationState("left");
  _animState4 = _ent->getAnimationState("right");
  _animState->setEnabled(false);   _animState2->setEnabled(false);
  _animState3->setEnabled(false);  _animState4->setEnabled(false);
  // TCR ------------ animaciones  
  _up = false; 
_down = false;
 _left = false; 
_rigth = false;
  
  _type = 1;
  _removeAll = false;

	_maxHealth = 100;
//<JRC
	//creamos y cargamos el sistema de particulas para el propulsor
	m_pTrailParticle = sceneManager->createParticleSystem("Particle_trail", "Trail");
	Ogre::SceneNode* pTrailNode = _sceneNode->createChildSceneNode("Node_PlayerTrail");
	pTrailNode->attachObject(m_pTrailParticle);
	pTrailNode->translate(-15, -1, 0); //colocarla en la parte de atras de la nave
	

//>
}

Player::~Player ()
{

}

Player::Player (const Player & obj)
{
  
  _state = 0;
  
}

void Player::onclick ()
{

  

}

void Player::mouseIn ()
{
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();

 


}

void Player::mouseOut ()
{
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  
}

void Player::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  Ogre::Vector3 position = _sceneNode->getPosition();
  
	float timeLFa = evt.timeSinceLastFrame;
	float n = 100  * timeLFa;
	float timeLF = 2*timeLFa;
	
 	  if (_up && position.y + n <= Y_MAX )  position.y += n;
	  if (_down && position.y - n >= Y_MIN) position.y -= n;
	  if (_left && position.x - n >= X_MIN) position.x -= n;
	  if (_rigth && position.x + n <= X_MAX) position.x += n;
	  
	// TCR ------------ animaciones  
  if (_up && !(_animState->getEnabled() || _animState2->getEnabled() 
    || _animState3->getEnabled() || _animState4->getEnabled())) {
    _animState->setTimePosition(0.0);
    _animState->setEnabled(true);
    _animState->setLoop(false);
    
  }
  
  if (_down && !(_animState->getEnabled() || _animState2->getEnabled() 
    || _animState3->getEnabled() || _animState4->getEnabled())) {
    _animState2->setTimePosition(0.0);
    _animState2->setEnabled(true);
    _animState2->setLoop(false);
    
  }
  
  if (!_up && _animState->getEnabled()) {
    if (_animState->getTimePosition() > 0.0)
      _animState->addTime(-timeLF);
    else
      _animState->setEnabled(false);
  }
  
  if (!_down && _animState2->getEnabled()) {
    if (_animState2->getTimePosition() > 0.0)
      _animState2->addTime(-timeLF);
    else
      _animState2->setEnabled(false);
  }
  
  if (!_left && _animState3->getEnabled()) {
    if (_animState3->getTimePosition() > 0.0)
      _animState3->addTime(-timeLF);
    else
      _animState3->setEnabled(false);
  }
  
  if (!_rigth && _animState4->getEnabled()) {
    if (_animState4->getTimePosition() > 0.0)
      _animState4->addTime(-timeLF);
    else
      _animState4->setEnabled(false);
  }
  
  
   if (_animState->getEnabled() && !_animState->hasEnded() && _up) {    
     _animState->addTime(timeLF);     
   }
   
   if (_animState2->getEnabled() && !_animState2->hasEnded() && _down) {    
     _animState2->addTime(timeLF);     
   }  
   
   if (_animState3->getEnabled() && !_animState3->hasEnded() && _left) {    
     _animState3->addTime(timeLF);     
   }  
   
   if (_animState4->getEnabled() && !_animState4->hasEnded() && _rigth) {    
     _animState4->addTime(timeLF);     
   }  
      // TCR ------------ animaciones  
   
   
   
	  _sceneNode->setPosition(position);
	  _up = false; _down = false; _left = false; _rigth = false;
  
}

void Player::moveUp ()
{
	
	_up = true;

}

void Player::moveDown ()
{
	
	_down = true;

}

void Player::moveLeft ()
{
	
	_left = true;

}

void Player::moveRigth ()
{
	
	_rigth = true;

}

void Player::onIntersect(GameObject* gameObject, Timer* timer)
{
	GameManager *gameManager = GameManager::getSingleton ();
	if (gameObject->_type != ITEM)
	{
		int damage = 0;
		if (gameObject->_type == SHOT )
		{
			Shot* shot = static_cast<Shot*>(gameObject);
			if (shot->_parent->_type != PLAYER)
				damage = shot->_damage;
		} 
		
		else if (gameObject->_type == ENEMY)
		{
			Enemy* enemy = static_cast<Enemy*>(gameObject);
			damage = enemy->_damage;
		}

		else if (gameObject->_type == PIECE)
		{
			Piece* piece = static_cast<Piece*>(gameObject);
			damage = piece->_damage;
		}

		_health -= damage;

		if (_health <= 0) {
		
			GameState* gameState = gameManager->getGameState();
			gameState->win = false;		
			new Explosion( gameManager->getSceneManager(), _sceneNode->getPosition(), 2.0f, "ExplosionPlayer", "HugeExplosion");
			gameState->endgame();
			_sceneNode->setVisible(false);
			_isIntersect = false;
			_update_quadtree = false;
			//_remove = true;
		}
	}
	
}

void Player::respawn()
{
	_health = 100;
	_weapon->_category = 1;
	_sceneNode->setPosition(0,0,0);
	_sceneNode->setVisible(true);
	_isIntersect = true;
	_update_quadtree = true;
			
}


