#if OGRE_PLATFORM != OGRE_PLATFORM_WIN32
#include <unistd.h>
#endif

#include "game/Shot.h"
#include "factory/GameManager.h"
#include "tools/extglobal.h"
#include "game/Explosion.h"

Shot::Shot (Ogre::SceneManager * sceneManager, const Ogre::String & name) : GameObject (sceneManager,
	    name)
{
 
  _type = SHOT;
  _inverter = 1;
  
}

Shot::~Shot ()
{

}

Shot::Shot (const Shot & obj)
{
  
  
  
}


void Shot::update (const Ogre::FrameEvent & evt, Timer* timer)
{
  /*Update del bucle principal por si queremos hacer animaciones */
  /*Aqui podemos llamar a cualquier clase a raiz de GameManager */
  GameManager *gameManager = GameManager::getSingleton ();
  GameState *gameState = gameManager->getGameState ();
  Ogre::Vector3 position = _sceneNode->getPosition();
  
	float n = (400  * evt.timeSinceLastFrame) * _inverter;
 	  
	  _sceneNode->translate(n,0,0);


  if (isOutlimit())
	  _remove = true;
}


void Shot::onIntersect(GameObject* gameObject, Timer* timer)
{
	//GameManager *gameManager = GameManager::getSingleton ();
	if (_parent->_type != gameObject->_type && gameObject->_type != ITEM){

			//<JRC: provisional, particulas al destruir los disparos por colision, igual conviene 			meterlo en una funcion tipo destroy por claridad	
			Ogre::SceneManager* sceneMgr =	 GameManager::getSingleton()->getSceneManager();
			Ogre::Vector3 position = _sceneNode->getPosition();
			position.z += 20;		
			new Explosion(sceneMgr, position, 0.3f, "EnergyShot","nosound");
			//>
			_remove = true;
	}
	 
	
}


