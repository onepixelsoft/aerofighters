#include "game/Weapon.h"
#include "game/Shot.h"
#include "factory/GameManager.h"
#include "game/GameState.h"
#include "factory/GameFactory.h"
#include "tools/MyConfig.h"
#include "tools/extglobal.h"

Weapon::Weapon()
{
	_type = WEAPON;
	_tShoot = MyConfig::getInstance().getValueAsInt("Items/Timeshoot");
	_wait = 0;
	_category = 1;
	_inverter = 1;
}

Weapon::~Weapon()
{

}

Weapon::Weapon(const Shot &card)
{
}

void Weapon::shoot(unsigned long timer)
{

	if ((timer % _tShoot) == 0 || !_wait || timer - _wait >= _tShoot) {
		_wait = timer;
		GameManager* gameManager = GameManager::getSingleton();
		GameState* gameState = gameManager->getGameState();
		GameFactory* gameFactory = gameManager->getGameFactory();
		if (_category == 1) {
	
			Shot* shot = gameFactory->createShot(_shotType);
			shot->getSceneNode()->setPosition(_parent->getSceneNode()->getPosition());
			shot->_parent = _parent;
			shot->_inverter = _inverter;
			gameState->addWorld(shot);
			
				
		} else if (_category == 2) 
		{
			
			Ogre::Vector3 position = _parent->getSceneNode()->getPosition();
			Ogre::Vector3* newpos = new Ogre::Vector3(position.x,position.y-15,position.z);
			for(int i=0;i<25;i+=5)
			{
				Shot* shot = gameFactory->createShot(_shotType);
				shot->_parent = _parent;
				shot->_inverter = _inverter;
				shot->getSceneNode()->setPosition(newpos->x,newpos->y+i,newpos->z);
				gameState->addWorld(shot);
				
			}

			
		}
		//reproducir sonido de disparo	
		//gameManager->playFX("PlayerShot");	
	}
}
