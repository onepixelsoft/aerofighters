#include "tools/GameUtil.h"
#include "engine/GameObject.h"


template <> GameUtil * Ogre::Singleton < GameUtil >::msSingleton = 0;

GameUtil::GameUtil ()
{
  Ogre::Root * root = Ogre::Root::getSingletonPtr ();
  _mSceneManager = root->getSceneManager ("SceneManagerInstance1");
  _mRaySceneQuery = _mSceneManager->createRayQuery (Ogre::Ray ());

}

GameUtil::~GameUtil ()
{
}

GameUtil & 
	GameUtil::getSingleton ()
    
{
	if (!msSingleton)
		msSingleton = new GameUtil ();
	assert (msSingleton);

	return (*msSingleton);

}



GameUtil * 
	GameUtil::getSingletonPtr () 
{
	if (!msSingleton)
		msSingleton = new GameUtil ();
	assert (msSingleton);

	return msSingleton;

}

Ogre::MovableObject * GameUtil::getObjPoint (const OIS::MouseState & arg,
					     CEGUI::Point mousePos, uint32_t mask)
{

  Ogre::MovableObject * obj = 0;
  Ogre::Camera * mCamera = _mSceneManager->getCamera ("MainCamera");
  Ogre::Real tx = mousePos.d_x / (Ogre::Real) arg.width;
  Ogre::Real ty = mousePos.d_y / (Ogre::Real) arg.height;
  Ogre::Ray ray = mCamera->getCameraToViewportRay (tx, ty);
  _mRaySceneQuery->setRay (ray);
  _mRaySceneQuery->setQueryMask(mask);

  Ogre::RaySceneQueryResult & result = _mRaySceneQuery->execute ();
  Ogre::RaySceneQueryResult::iterator itr = result.begin ();

  if (itr != result.end () && itr->movable)
    {
      return itr->movable;
    }

  return obj;
}

bool GameUtil::intersection(Ogre::SceneNode* box_a, Ogre::SceneNode* box_b)
{
	Ogre::AxisAlignedBox aab = box_a->_getWorldAABB().intersection(box_b->_getWorldAABB());
	if(!aab.isNull())
	{
		return true;
	}

	return false;
}

bool GameUtil::getScreenspaceCoords(Ogre::MovableObject* object, Ogre::Camera* camera, Ogre::Vector2& result)
{
   if(!object->isInScene())
      return false;
 
   const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox(true);
 
   /**
   * If you need the point above the object instead of the center point:
   * This snippet derives the average point between the top-most corners of the bounding box
   * Ogre::Vector3 point = (AABB.getCorner(AxisAlignedBox::FAR_LEFT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::FAR_RIGHT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::NEAR_LEFT_TOP)
   *    + AABB.getCorner(AxisAlignedBox::NEAR_RIGHT_TOP)) / 4;
   */
 
   // Get the center point of the object's bounding box
   Ogre::Vector3 point = AABB.getCenter();
 
   // Is the camera facing that point? If not, return false
   Ogre::Plane cameraPlane = Ogre::Plane(Ogre::Vector3(camera->getDerivedOrientation().zAxis()), camera->getDerivedPosition());
   if(cameraPlane.getSide(point) != Ogre::Plane::NEGATIVE_SIDE)
      return false;
 
   // Transform the 3D point into screen space
   point = camera->getProjectionMatrix() * (camera->getViewMatrix() * point);
 
   // Transform from coordinate space [-1, 1] to [0, 1] and update in-value
   result.x = (point.x / 2) + 0.5f;
   result.y = 1 - ((point.y / 2) + 0.5f);
 
   return true;
}


	
