/* **********************************************************
** Importador NoEscape 1.0
** Curso de Experto en Desarrollo de Videojuegos 
** Escuela Superior de Informatica - Univ. Castilla-La Mancha
** Carlos Gonzalez Morcillo - David Vallejo Fernandez
************************************************************/

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <OgreVector3.h>
#include <OgreVector4.h>
#include <iostream>
#include <tools/Importer.h>

#include "game/Level.h"
#include "game/Pts.h"
#include "game/GameXML.h"
#include "game/GameObjectXML.h"


using namespace std;
using namespace xercesc;

template<> Importer* Ogre::Singleton<Importer>::msSingleton = 0;

Importer* Importer::getSingletonPtr(void)
{
	if (!msSingleton)
		msSingleton = new Importer();
    return msSingleton;
}
Importer& Importer::getSingleton(void)
{
	if (!msSingleton)
		msSingleton = new Importer();
    assert( msSingleton );  return ( *msSingleton );  
}

void
Importer::parseGame
(const char* path, GameXML* gameXML)
{
  // Inicialización.
  try {
    XMLPlatformUtils::Initialize();
  }
  catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    cout << "Error durante la inicialización! :\n"
	 << message << "\n";
    XMLString::release(&message);
    return;
  }

  XercesDOMParser* parser = new XercesDOMParser();
  parser->setValidationScheme(XercesDOMParser::Val_Always);

  // 'Parseando' el fichero xml...
  try {
    parser->parse(path);
  }
  catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    cout << "Excepción capturada: \n"
	 << message << "\n";
    XMLString::release(&message);
  }
  catch (const DOMException& toCatch) {
    char* message = XMLString::transcode(toCatch.msg);
    cout << "Excepción capturada: \n"
	 << message << "\n";
    XMLString::release(&message);
  }
  catch (...) {
    cout << "Excepción no esperada.\n" ;
    return;
  }

  DOMDocument* xmlDoc;
  DOMElement* elementRoot;

  try {
    // Obtener el elemento raíz del documento.
    xmlDoc = parser->getDocument(); 
    elementRoot = xmlDoc->getDocumentElement();

    if(!elementRoot)
      throw(std::runtime_error("Documento XML vacío."));

  }
  catch (xercesc::XMLException& e ) {
    char* message = xercesc::XMLString::transcode( e.getMessage() );
    ostringstream errBuf;
    errBuf << "Error 'parseando': " << message << flush;
    XMLString::release( &message );
    return;
  }
 
  XMLCh* level = XMLString::transcode("level");  
  
  

  // Procesando los nodos hijos del raíz...
  for (XMLSize_t i = 0; 
       i < elementRoot->getChildNodes()->getLength(); 
       ++i ) {
    
    DOMNode* node = elementRoot->getChildNodes()->item(i);

    if (node->getNodeType() == DOMNode::ELEMENT_NODE) {
      // Nodo <camera>?
      if (XMLString::equals(node->getNodeName(), level))
		parseLevel(node, gameXML);
	}
    
  }
  // Liberar recursos.
 
 
  XMLString::release(&level);
  delete parser;
}

void
Importer::parseLevel
(xercesc::DOMNode* levelNode, GameXML* gameXML)
{
  // Atributos de la cámara.
  DOMNamedNodeMap* attributes = levelNode->getAttributes();
  DOMNode* indexId = attributes->getNamedItem(XMLString::transcode("id"));
  DOMNode* indexNext = attributes->getNamedItem(XMLString::transcode("nextlevel"));

  int id = atoi(XMLString::transcode(indexId->getNodeValue()));
  int nextlevel = atoi(XMLString::transcode(indexNext->getNodeValue()));

  // Instanciar la cámara.
	Level* level = new Level();
	level->setId(id);
	level->setNextlevel(nextlevel);

  XMLCh* pts = XMLString::transcode("pts");  

  // Procesar el path de la cámara.
  for (XMLSize_t i = 0; i < levelNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* ptsNode = levelNode->getChildNodes()->item(i);

   
    if (ptsNode->getNodeType() == DOMNode::ELEMENT_NODE &&
	XMLString::equals(ptsNode->getNodeName(), pts))
      parsePts(ptsNode, level);

  }

  XMLString::release(&pts);

  gameXML->_levels.push_back(level);
  
}

void
Importer::parsePts
(xercesc::DOMNode* ptsNode, Level* level)
{
  // Atributos de la cámara.
  DOMNamedNodeMap* attributes = ptsNode->getAttributes();
  DOMNode* indexId = attributes->getNamedItem(XMLString::transcode("id"));
  DOMNode* indexNext = attributes->getNamedItem(XMLString::transcode("nextpts"));

  
  

  int id = atoi(XMLString::transcode(indexId->getNodeValue()));
  int nextpts = atoi(XMLString::transcode(indexNext->getNodeValue()));
 

  // Instanciar la cámara.
	Pts* pts = new Pts();
	pts->setId(id);
	pts->setNextpts(nextpts);

  XMLCh* pts_ch = XMLString::transcode("gameobject");  

  // Procesar el path de la cámara.
  for (XMLSize_t i = 0; i < ptsNode->getChildNodes()->getLength(); ++i ) {

    DOMNode* goNode = ptsNode->getChildNodes()->item(i);

   
    if (ptsNode->getNodeType() == DOMNode::ELEMENT_NODE &&
	XMLString::equals(goNode->getNodeName(), pts_ch))
      parseGO(goNode, pts);

  }

  XMLString::release(&pts_ch);

  // Cada vez que se parsea una cámara,
  // se añade a la escena.
  //scene->addCamera(camera);
  level->_pts.push_back(pts);
}

void
Importer::parseGO
(xercesc::DOMNode* goNode, Pts* pts)
{
  // Atributos de la cámara.
  DOMNamedNodeMap* attributes = goNode->getAttributes();
 
  DOMNode* indexType = attributes->getNamedItem(XMLString::transcode("type"));

   DOMNode* indexX = attributes->getNamedItem(XMLString::transcode("x"));
  DOMNode* indexY = attributes->getNamedItem(XMLString::transcode("y"));
   DOMNode* indexZ = attributes->getNamedItem(XMLString::transcode("z"));
    DOMNode* indexCategory = attributes->getNamedItem(XMLString::transcode("category"));
  DOMNode* indexMoveai = attributes->getNamedItem(XMLString::transcode("move_ai"));
   DOMNode* indexShotai = attributes->getNamedItem(XMLString::transcode("shot_ai"));
  DOMNode* indexVelocity = attributes->getNamedItem(XMLString::transcode("velocity"));
   DOMNode* indexWeapon= attributes->getNamedItem(XMLString::transcode("weapon"));
    DOMNode* indexNumber= attributes->getNamedItem(XMLString::transcode("number"));
  DOMNode* indexRemove = attributes->getNamedItem(XMLString::transcode("remove"));
   DOMNode* indexExplosion= attributes->getNamedItem(XMLString::transcode("explosion"));

    DOMNode* indexXlow = attributes->getNamedItem(XMLString::transcode("x_low"));
   DOMNode* indexXhigh= attributes->getNamedItem(XMLString::transcode("x_high"));
    DOMNode* indexYlow = attributes->getNamedItem(XMLString::transcode("y_low"));
   DOMNode* indexYhigh= attributes->getNamedItem(XMLString::transcode("y_high"));

   DOMNode* indexWait= attributes->getNamedItem(XMLString::transcode("wait"));
 

  GameObjectXML* go_xml = new GameObjectXML();

  go_xml->_type = atoi(XMLString::transcode(indexType->getNodeValue()));
  go_xml->_x = atof(XMLString::transcode(indexX->getNodeValue()));
  go_xml->_y = atof(XMLString::transcode(indexY->getNodeValue()));
  go_xml->_z = atof(XMLString::transcode(indexZ->getNodeValue()));
  go_xml->_category = atoi(XMLString::transcode(indexCategory->getNodeValue()));
  go_xml->_move_ai = atoi(XMLString::transcode(indexMoveai->getNodeValue()));
  go_xml->_shot_ai = atoi(XMLString::transcode(indexShotai->getNodeValue()));
  go_xml->_velocity = atoi(XMLString::transcode(indexVelocity->getNodeValue()));

  go_xml->_weapon = atoi(XMLString::transcode(indexWeapon->getNodeValue()));
  go_xml->_number = atoi(XMLString::transcode(indexNumber->getNodeValue()));
  go_xml->_remove = atoi(XMLString::transcode(indexRemove->getNodeValue()));
  go_xml->_explosion = atoi(XMLString::transcode(indexExplosion->getNodeValue()));

  go_xml->_x_low = atoi(XMLString::transcode(indexXlow->getNodeValue()));
  go_xml->_x_high = atoi(XMLString::transcode(indexXhigh->getNodeValue()));
  go_xml->_y_low = atoi(XMLString::transcode(indexYlow->getNodeValue()));
  go_xml->_y_high = atoi(XMLString::transcode(indexYhigh->getNodeValue()));

  go_xml->_wait = atoi(XMLString::transcode(indexWait->getNodeValue()));

 pts->_gosXml.push_back(go_xml);
 if (go_xml->_type == 2)
	pts->_lengthGos += go_xml->_number;
}
