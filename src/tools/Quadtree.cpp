#include "tools/Quadtree.h"
#include "engine/GameObject.h"
#include <Ogre.h>
using namespace std;

Quadtree::Quadtree( float _x, float _y, float _width, float _height, int _level, int _maxLevel ) :
	x		( _x ),
	y		( _y ),
	width	( _width ),
	height	( _height ),
	level	( _level ),
	maxLevel( _maxLevel )
{
	if ( level == maxLevel ) {
		return;
	}

	NW = new Quadtree( x, y, width / 2.0f, height / 2.0f, level+1, maxLevel );
	NE = new Quadtree( x + width / 2.0f, y, width / 2.0f, height / 2.0f, level+1, maxLevel );
	SW = new Quadtree( x, y + height / 2.0f, width / 2.0f, height / 2.0f, level+1, maxLevel );
	SE = new Quadtree( x + width / 2.0f, y + height / 2.0f, width / 2.0f, height / 2.0f, level+1, maxLevel );
}

Quadtree::~Quadtree()
{
	if ( level == maxLevel ) {
		objects.clear();
	} else {

		delete NW;
		delete NE;
		delete SW;
		delete SE;
	}
}

void Quadtree::AddObject( GameObject *object ) {

	Ogre::Vector2* nlt = new Ogre::Vector2(0,0);
	Ogre::Vector2* nrt = new Ogre::Vector2(0,0);
	Ogre::Vector2* nlb = new Ogre::Vector2(0,0);
	Ogre::Vector2* nrb = new Ogre::Vector2(0,0);
	object->getPosAll(*nlt,*nrt,*nlb,*nrb);

	_AddObject(object, nlt, nrt, nlb, nrb);
	
	delete nlt;
	delete nrt;
	delete nlb;
	delete nrb;
	
}

void Quadtree::RemoveObject( GameObject *object ) {

	Ogre::Vector2* nlt = new Ogre::Vector2(0,0);
	Ogre::Vector2* nrt = new Ogre::Vector2(0,0);
	Ogre::Vector2* nlb = new Ogre::Vector2(0,0);
	Ogre::Vector2* nrb = new Ogre::Vector2(0,0);
	object->getPosAll(*nlt,*nrt,*nlb,*nrb);

	_RemoveObject(object, nlt, nrt, nlb, nrb);
	
	delete nlt;
	delete nrt;
	delete nlb;
	delete nrb;
}

void Quadtree::_AddObject( GameObject *object, Ogre::Vector2* nlt, Ogre::Vector2* nrt, Ogre::Vector2* nlb, Ogre::Vector2* nrb ) {
	if ( level == maxLevel ) {
		objects.push_back( object );
		return;
	}
	

	if ( Contains( NW, object, nlt, nrt, nlb, nrb ) ) {
		NW->_AddObject( object, nlt, nrt, nlb, nrb);
	}  if ( Contains( NE, object, nlt, nrt, nlb, nrb ) ) {
		NE->_AddObject( object, nlt, nrt, nlb, nrb);
	}  if ( Contains( SW, object, nlt, nrt, nlb, nrb ) ) {
		SW->_AddObject( object, nlt, nrt, nlb, nrb);
	}  if ( Contains( SE, object, nlt, nrt, nlb, nrb ) ) {
		SE->_AddObject( object, nlt, nrt, nlb, nrb);
	}
	
}

void Quadtree::_RemoveObject( GameObject *object, Ogre::Vector2* nlt, Ogre::Vector2* nrt, Ogre::Vector2* nlb, Ogre::Vector2* nrb ) {
	if ( level == maxLevel ) {
		objects.erase(std::remove(objects.begin(), objects.end(), object), objects.end());
		return;
	}
	

	if ( Contains( NW, object, nlt, nrt, nlb, nrb ) ) {
		NW->_RemoveObject( object, nlt, nrt, nlb, nrb);
	}  if ( Contains( NE, object, nlt, nrt, nlb, nrb ) ) {
		NE->_RemoveObject( object, nlt, nrt, nlb, nrb);
	}  if ( Contains( SW, object, nlt, nrt, nlb, nrb ) ) {
		SW->_RemoveObject( object, nlt, nrt, nlb, nrb);
	}  if ( Contains( SE, object, nlt, nrt, nlb, nrb ) ) {
		SE->_RemoveObject( object, nlt, nrt, nlb, nrb);
	}
	
}

vector<GameObject*> Quadtree::GetObjectsAt( GameObject* gameObject ) {

	if ( level == maxLevel ) {
		return objects;
	}

	Ogre::Vector2* nlt = new Ogre::Vector2(0,0);
	Ogre::Vector2* nrt = new Ogre::Vector2(0,0);
	Ogre::Vector2* nlb = new Ogre::Vector2(0,0);
	Ogre::Vector2* nrb = new Ogre::Vector2(0,0);
	gameObject->getPosAll(*nlt,*nrt,*nlb,*nrb);
	
	vector<GameObject*> returnObjects, childReturnObjects;
	//if ( !objects.empty() ) {
		//returnObjects = objects;
	//}
	if ( isEnterE(nlt) || isEnterE(nrt) || isEnterE(nlb) || isEnterE(nrb) ) {
		if ( isEnterS(nlt) || isEnterS(nrt) || isEnterS(nlb) || isEnterS(nrb) ) {
			childReturnObjects = SE->GetObjectsAt( gameObject );
			returnObjects.insert( returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end() );
			delete nlt;
			delete nrt;
			delete nlb;
			delete nrb;
			return returnObjects;
		} else if ( isEnterN(nlt) || isEnterN(nrt) || isEnterN(nlb) || isEnterN(nrb) ) {
			childReturnObjects = NE->GetObjectsAt( gameObject );
			returnObjects.insert( returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end() );
			delete nlt;
			delete nrt;
			delete nlb;
			delete nrb;
			return returnObjects;
		}
	} else if ( isEnterW(nlt) || isEnterW(nrt) || isEnterW(nlb) || isEnterW(nrb) ) {
		if ( isEnterS(nlt) || isEnterS(nrt) || isEnterS(nlb) || isEnterS(nrb) ) {
			childReturnObjects = SW->GetObjectsAt( gameObject );
			returnObjects.insert( returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end() );
			delete nlt;
			delete nrt;
			delete nlb;
			delete nrb;
			return returnObjects;
		} else if ( isEnterN(nlt) || isEnterN(nrt) || isEnterN(nlb) || isEnterN(nrb) ) {
			childReturnObjects = NW->GetObjectsAt( gameObject );
			returnObjects.insert( returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end() );
			delete nlt;
			delete nrt;
			delete nlb;
			delete nrb;
			return returnObjects;
		}
	}

	delete nlt;
	delete nrt;
	delete nlb;
	delete nrb;
	
	return returnObjects;
}

bool Quadtree::isEnterE (Ogre::Vector2* pos)
{
	return (pos->x >= x + width / 2.0f && pos->x <= x + width);
}

bool Quadtree::isEnterW (Ogre::Vector2* pos)
{
	return ( pos->x >= x && pos->x <= x + width / 2.0f );
}

bool Quadtree::isEnterN (Ogre::Vector2* pos)
{
	return ( pos->y >= y && pos->y <= y + height / 2.0f );
}

bool Quadtree::isEnterS (Ogre::Vector2* pos)
{
	return ( pos->y >= y + height / 2.0f && pos->y < y + height );
}

void Quadtree::Clear() {
	if ( level == maxLevel ) {
		objects.clear();
		return;
	} else {
		NW->Clear();
		NE->Clear();
		SW->Clear();
		SE->Clear();
	}
	if ( !objects.empty() ) {
		objects.clear();
	}
}

bool Quadtree::Contains( Quadtree *child, GameObject *object, Ogre::Vector2* nlt, Ogre::Vector2* nrt, Ogre::Vector2* nlb, Ogre::Vector2* nrb ) {

	bool result = false;

	/*
	result = !( nlt->x < child->x ||
				nlt->y < child->y ||
				nlt->x > child->width  ||
				nlt->y  > child->height ||
				x + w < child->x ||
				y + h < child->y ||
				x + w > child->x + child->width ||
				y + h > child->y + child->height );
				*/
	result = isContains(child, nlt);
				
	if (!result) {
		result = isContains(child, nrt);
	}

	if (!result) {
		result = isContains(child, nlb);
	}

	if (!result) {
		result = isContains(child, nrb);
	}
	
	return result;
		
}

bool Quadtree::isContains (Quadtree *child, Ogre::Vector2* pos)
{
	return !( pos->x < child->x ||
				pos->y < child->y ||
				pos->x > child->x + child->width  ||
				pos->y  > child->y + child->height);
}