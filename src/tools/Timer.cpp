#include "tools/Timer.h"



template <> Timer * Ogre::Singleton < Timer >::msSingleton = 0;

Timer::Timer ()
{
  
  _timer = 0;
  _pause = 0;
  _t = 0;
}

Timer::~Timer ()
{
	delete _timer;
}

Timer & 
	Timer::getSingleton ()
    
{
	if (!msSingleton)
		msSingleton = new Timer ();
	assert (msSingleton);

	return (*msSingleton);

}



Timer * 
	Timer::getSingletonPtr () 
{
	if (!msSingleton)
		msSingleton = new Timer ();
	assert (msSingleton);

	return msSingleton;

}


void Timer::reset()
{
	_pause = 0;
	delete _timer;
	_timer = 0;
	_t = 0;

}

void Timer::pause()
{
	
	//_pause += _t;
	delete _timer;
	_timer = 0;
}


unsigned long Timer::getCurrentTimer() const
{
	return _t;
}

Ogre::String Timer::getTimerStr()
{
	
	if (!_timer) {
		_timer = new Ogre::Timer();
		_timer->reset();
	}
	#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			_t += _timer->getMillisecondsCPU();
	#else
			_t += _timer->getMilliseconds();
	#endif

	 
	 _timer->reset();
	
	const long hours        = ((_t / 1000) / 60) / 60;
	
    const long minutes      = ((_t / 1000) / 60) - (60 * hours);

	const long seconds      = ((_t / 1000)) - (60 * minutes);
	
	
	char buf[40];
    sprintf(buf, "%02ld:%02ld:%02ld", 
        hours, minutes, seconds);

    return buf;
	
}

	
